$(document).ready(function() {
	initEvent();
});

function initEvent(){
	
	$("#googleDrive a").bind("click",function(){
		var id = $(this).attr("id");
		openDriveFolder(id);
	});
	
	$("#googleDriveLooncode a").bind("click",function(){
		var id = $(this).attr("id");
		openDriveFolderForLooncode(id);
	});

	$('#btn-upload, #btn-uploadCSV, #btn-generate, #btn-process, #btn-send, #btn-download, #btn-generateIonFile, #btn-downloadLONFile,#btn-downloadOmniPrint').on("click",function(){
		
		var id = $(this).attr("id").split('-');
		
		switch (id[1]){
						case 'upload' : {
						};break;
						case 'uploadCSV' : {
						};break;
						case 'generate' : openPeriodScreen();
											break;
						case 'process' : openProcessScreen();
											break;
						case 'send' 	: openSendScreen();
											break;
						case 'download' : openDownloadScreen();
											break;
						case 'generateIonFile' : openTemplateScreen();
						break;
						case 'downloadLONFile' : openDownloadLONScreen();
						break;
						case 'downloadOmniPrint' : openDownloadOmniScreen();
						break;
											
		}
		event.preventDefault()
	})
	
	$("#period").numeric({ decimal: false }, function() { 
		this.value = ""; 
		this.focus(); 
	});
	
	$("#irisPeriod").numeric({ decimal: false }, function() { 
		this.value = ""; 
		this.focus(); 
	});
	
	$("#csvPeriod").numeric({ decimal: false }, function() { 
		this.value = ""; 
		this.focus(); 
	});
	
	$('#btn-uploadCsvSubmit').on("click",function() {
		var clientName = $('#selectedClientNm').html();
		
		if(clientName == "Iris"){			
			$('#uploadCsvForm').find('#irisPeriod,select').not("[type=submit]").jqBootstrapValidation();
			var invalid = $('#uploadCsvForm').find('#irisPeriod,select').not("[type=submit]").jqBootstrapValidation('hasErrors')
			if(invalid) return;
			
			if(!checkIrisPeriod($("#irisPeriod").val()))
				return;
		}
		else{			
			$('#uploadCsvForm').find('#csvPeriod,select').not("[type=submit]").jqBootstrapValidation();
			var invalid = $('#uploadCsvForm').find('#csvPeriod,select').not("[type=submit]").jqBootstrapValidation('hasErrors')
			if(invalid) return;
			
			if(!checkCsvPeriod($("#csvPeriod").val()))
				return;
		}
		$("#errorMsg_uploadCsv").html("uploading,  <img src='img/ajax-loader2.gif' alt='Loading' />"); //this should replace with a loading bar
		$("#errorMsgDiv_uploadCsv").show();
		uploadCsvFile();
	});

	$('#btn-uploadSubmit').on("click",function() {
		
		$("#errorMsgDiv_upload").hide();
		$('#uploadForm').find('input,select').not("[type=submit]").jqBootstrapValidation();
		var invalid = $('#uploadForm').find('input,select').not("[type=submit]").jqBootstrapValidation('hasErrors')
		if(invalid) return;
		

		$("#errorMsg_upload").html("uploading,  <img src='img/ajax-loader2.gif' alt='Loading' />"); //this should replace with a loading bar
		$("#errorMsgDiv_upload").show();
		uploadPerFile();
	});
	
	$('#btn-periodSubmit').on("click",function() {
		$("#errorMsgDiv").hide();
		$('#periodForm').find('input,select').not("[type=submit]").jqBootstrapValidation();
		var invalid = $('#periodForm').find('input,select').not("[type=submit]").jqBootstrapValidation('hasErrors')
		if(invalid) return;
		
		if(!checkPeriod($("#period").val()))
			return;
		
		var year = parseInt($("#period").val().substring(0,4));
		var month = parseInt($("#period").val().substring(4,6));
		
		$("#errorMsg").html("calendar generating, pls wait...<img src='img/ajax-loader2.gif' alt='Loading' />"); //this should replace with a loading bar

		$("#errorMsgDiv").show();
		genCalendar($("#perfile").val(),year,month);
	});
	
	
	$('#btn-processSubmit').on("click",function() {
		$("#errorMsgDiv_process").hide();
		$('#processForm').find('input,select').not("[type=submit]").jqBootstrapValidation();
		var invalid = $('#processForm').find('input,select').not("[type=submit]").jqBootstrapValidation('hasErrors')
		if(invalid) return;
		
		
		var selectionAvailable = $("#generateOptionContainer").is(":visible"); 
		if(selectionAvailable){
			var selectedOpt = $("#generateOption").val();
			if(selectedOpt == "t"){
				$("#errorMsg_process").html("Tikkingen generating, pls wait...<img src='img/ajax-loader2.gif' alt='Loading' />"); //this should replace with a loading bar
				$("#errorMsgDiv_process").show();
				genTikkingen($("#calendar").val(),$("#calendar option:selected").html());
			}else if(selectedOpt == "l"){
				$("#errorMsg_process").html("lon generating, pls wait...<img src='img/ajax-loader2.gif' alt='Loading' />"); //this should replace with a loading bar
				$("#errorMsgDiv_process").show();
				genLON($("#calendar").val(),$("#calendar option:selected").html(),'generate');
			}else{
				
			}
		}else{
			var clientName = $('#selectedClientNm').html();
			//if(clientName == "Hilma" || clientName == "Demo" || clientName == "Iris"){
			if(clientName == "Iris"){
				$("#errorMsg_process").html("lon and Tikkingen generating, pls wait...<img src='img/ajax-loader2.gif' alt='Loading' />"); //this should replace with a loading bar
				$("#errorMsgDiv_process").show();
				genLON($("#calendar").val(),$("#calendar option:selected").html(),'generate');
				genTikkingen($("#calendar").val(),$("#calendar option:selected").html());
			}else{
				$("#errorMsg_process").html("lon generating, pls wait...<img src='img/ajax-loader2.gif' alt='Loading' />"); //this should replace with a loading bar
				$("#errorMsgDiv_process").show();
				genLON($("#calendar").val(),$("#calendar option:selected").html(),'generate');
			}
			
		}
		
	});
	
	$('#btn-processValidate').on("click",function() {
		$("#logMsgDiv_process").hide();
		$("#errorMsgDiv_process").hide();
		$('#processForm').find('input,select').not("[type=submit]").jqBootstrapValidation();
		var invalid = $('#processForm').find('input,select').not("[type=submit]").jqBootstrapValidation('hasErrors')
		if(invalid) return;
		
		$("#errorMsg_process").html("Validating Calendar, pls wait...<img src='img/ajax-loader2.gif' alt='Loading' />"); //this should replace with a loading bar
		$("#errorMsgDiv_process").show();
		genLON($("#calendar").val(),$("#calendar option:selected").html(),'validate');
	});
	
	
	$('#btn-sendSubmit').on("click",function() {
		$("#errorMsgDiv_send").hide();
		$('#sendForm').find('input,select').not("[type=submit]").jqBootstrapValidation();
		var invalid = $('#sendForm').find('input,select').not("[type=submit]").jqBootstrapValidation('hasErrors')
		if(invalid) return;
		
		$("#errorMsg_send").html("lon sending, pls wait...<img src='img/ajax-loader2.gif' alt='Loading' />"); //this should replace with a loading bar
		$("#errorMsgDiv_send").show();
		sendLON($("#lon").val(), $("#sendEmail").val());
	});
	
	$('#btn-downloadSubmit').on("click",function() {
		$("#errorMsgDiv_download").hide();
		$('#downloadForm').find('input,select').not("[type=submit]").jqBootstrapValidation();
		
		var selectionAvailable = $("#downloadOptionContainer").is(":visible"); 
		if(selectionAvailable){
			var optionVal = $("#downloadOption").val();
			if(optionVal == "t"){
				var invalid = $('#tikkingenDownload').find('input,select').not("[type=submit]").jqBootstrapValidation('hasErrors')
				if(invalid) return;
				$("#errorMsg_download").html("Tikkingen downloading, pls wait...<img src='img/ajax-loader2.gif' alt='Loading' />"); 
				$("#errorMsgDiv_download").show();
				downloadLON($("#tikkingenDownload").val(), false);
				$(this).attr('download', 'ExportToExcel.xls') // set file name (you want to put formatted date here)
	               .attr('href', "")                     // data to download
	               .attr('target', '_blank')              // open in new window (optional)
	             ;
			}else if(optionVal == "l"){
				var invalid = $('#lonDownloadContainer').find('input,select').not("[type=submit]").jqBootstrapValidation('hasErrors')
				if(invalid) return;
				$("#errorMsg_download").html("lon downloading, pls wait...<img src='img/ajax-loader2.gif' alt='Loading' />"); 
				$("#errorMsgDiv_download").show();
				downloadLON($("#lonDownload").val(),false);
			}else{
				
			}
			
		}else{
			$("#errorMsg_download").html("lon downloading, pls wait...<img src='img/ajax-loader2.gif' alt='Loading' />"); 
			$("#errorMsgDiv_download").show();
			downloadLON($("#lonDownload").val(),false);
		}
		
		
	});
	
	
	$('#btn-downloadSpreadsheetSubmit').on("click",function() {
		$("#errorMsgDiv_download").hide();
		$('#downloadForm').find('input,select').not("[type=submit]").jqBootstrapValidation();
		
		var selectionAvailable = $("#downloadOptionContainer").is(":visible"); 
		if(selectionAvailable){
			var optionVal = $("#downloadOption").val();
			if(optionVal == "t"){
				var invalid = $('#tikkingenDownload').find('input,select').not("[type=submit]").jqBootstrapValidation('hasErrors')
				if(invalid) return;
				$("#errorMsg_download").html("Tikkingen downloading, pls wait...<img src='img/ajax-loader2.gif' alt='Loading' />"); 
				$("#errorMsgDiv_download").show();
				downloadLON($("#tikkingenDownload").val(), true);
				$(this).attr('download', 'ExportToExcel.xls') // set file name (you want to put formatted date here)
	               .attr('href', "")                     // data to download
	               .attr('target', '_blank')              // open in new window (optional)
	             ;
			}else if(optionVal == "l"){
				var invalid = $('#lonDownloadContainer').find('input,select').not("[type=submit]").jqBootstrapValidation('hasErrors')
				if(invalid) return;
				$("#errorMsg_download").html("lon downloading, pls wait...<img src='img/ajax-loader2.gif' alt='Loading' />"); 
				$("#errorMsgDiv_download").show();
				downloadLON($("#lonDownload").val(),true);
			}else{
				
			}
			
		}else{
			$("#errorMsg_download").html("lon downloading, pls wait...<img src='img/ajax-loader2.gif' alt='Loading' />"); 
			$("#errorMsgDiv_download").show();
			downloadLON($("#lonDownload").val(),true);
		}
		
		
	});
	
	$('#btn-downloadLONSubmit').on("click",function() {
		$("#errorMsg_download").html("lon downloading, pls wait...<img src='img/ajax-loader2.gif' alt='Loading' />"); 
		$("#errorMsgDiv_download").show();
		downloadLON($("#lonTemplateDownload").val(),false, true);
	});
	
	$('#btn-downloadLONSpreadsheetSubmit').on("click",function() {
		$("#errorMsg_download").html("lon downloading, pls wait...<img src='img/ajax-loader2.gif' alt='Loading' />"); 
		$("#errorMsgDiv_download").show();
		downloadLON($("#lonTemplateDownload").val(),true, true);
	});
	
	$('#btn-downloadOmniSubmit').on("click",function() {
		var newTab = window.open('https://docs.google.com/spreadsheets/d/'+$("#omniTemplateDownload").val()+'/edit#gid=0', '_blank');
	    newTab.location;
	    $("#downloadOmniPrintScreen").modal("hide");
	});
	
	$('#btn-newUploadSubmit').on("click",function() {
		var invalid = $('#mapPeriod').find('input,select').not("[type=submit]").jqBootstrapValidation('hasErrors')
		if(invalid) return;
		var year = parseInt($("#mapPeriod").val().substring(0,4));
		var month = parseInt($("#mapPeriod").val().substring(4,6));
		
		if(month>12 || $("#mapPeriod").val()==null || $("#mapPeriod").val()==""){
			$("#errorMsg_templateUpload").html("Please check the field/value accordingly"); //this should replace with a loading bar
			$("#errorMsg_templateUpload").show();
			return;
		}else{
			$("#errorMsg_templateUpload").html("Template generating, pls wait...<img src='img/ajax-loader2.gif' alt='Loading' />"); //this should replace with a loading bar

			$("#errorMsgDiv_templateUpload").show();
			uploadOmniFile(year,month);		
			
		}
		
		
		
	
	});
	
	$('#btn-newGenerateLONSubmit').on("click",function() {
		var invalid = $('#mapPeriod').find('input,select').not("[type=submit]").jqBootstrapValidation('hasErrors')
		if(invalid) return;
		var year = parseInt($("#mapPeriod").val().substring(0,4));
		var month = parseInt($("#mapPeriod").val().substring(4,6));
		
		/*if(month>12 || $("#mapPeriod").val()==null || $("#mapPeriod").val()==""){
			$("#errorMsg_templateLONUpload").html("Please check the field/value accordingly"); //this should replace with a loading bar
			$("#errorMsg_templateLONUpload").show();
			return;
		}else{*/
			var correctieaangifte = 0;
			var generatie =0;
			var bedragenDoorsturen ="";
			if($('#correctieaangifte:checked').length>0){
				correctieaangifte = "1";
			}
			if($('#generatie:checked').length>0){
				generatie = "1";
			}
			if($('#bedragenDoorsturen:checked').length>0){
				bedragenDoorsturen = "0";
			}
			
			var typeOfLON = $("#typeOfLON").val();
			var hoofdfirma =  $("#hoofdfirma").val();
			var beschrijvingLON =  $("#beschrijvingLON").val();
			$("#errorMsg_templateLONUpload").html("Template LON generating, pls wait...<img src='img/ajax-loader2.gif' alt='Loading' />"); //this should replace with a loading bar

			$("#errorMsg_templateLONUpload").show();
			generateTemplateLONFile($("#templatefile").val(),year,month,correctieaangifte,generatie,typeOfLON,bedragenDoorsturen,hoofdfirma,beschrijvingLON);
		//}
		
	});
	
	$('#downloadOption').on("change",function() {
		var value = $('#downloadOption').val();
		if(value == "t"){
			$("#tikkingenDownloadContainer").show();
			$("#lonDownloadContainer").hide();
			$.ajax
			({
				cache: false,
				type: "GET",
				url: "/time",
				dataType: 'json',
				data: { 
					method : 'retrieveTikkingen' },
				async: true,
				success: function (response)  {
					if (response) {
						$('#tikkingenDownload').empty();
						$.each(response, function (i) {
							var name = response[i].title;
							var docID = response[i].docId;
							if(name)
								$('#tikkingenDownload')
								    .append($("<option></option>")
								    .attr("value",docID)
								    .text(name)); 
						});
						if(clientName == "Iris"){
							$("#tikkingenDownload").html($('#tikkingenDownload option').sort(function(x, y) {
					            return $(y).text() < $(x).text() ? -1 : 1;
					        }));	
						}else{
							$("#tikkingenDownload").html($('#tikkingenDownload option').sort(function(x, y) {
					            return $(x).text() < $(y).text() ? -1 : 1;
					        }));
						}
						

				        $("#tikkingenDownload").get(0).selectedIndex = 0;		
					}
				},error: function(response){
					globalErrorMethod(response);
				}
			});
			
		}else if(value == "l"){
			$("#tikkingenDownloadContainer").hide();
			$("#lonDownloadContainer").show();
		}else{
			$("#tikkingenDownloadContainer").hide();
			$("#lonDownloadContainer").hide();
		}
	});
	
	$('#generateOption').on("change",function() {
		var value = $('#generateOption').val();
		if(value == "t"){
			$("#btn-processValidate").hide();
			$("#logMsgDiv_process").hide();
		}else{
			$("#btn-processValidate").show();			
		}
	});
	
	var clientName = $('#selectedClientNm').html();
	//if(clientName == "Hilma" || clientName == "Demo" || clientName == "Iris"){
	if(clientName == "Hilma" || clientName == "Demo"){
		$("#btn-processValidate").show();
	}else{
		$("#btn-processValidate").hide();
	}
	
	if(clientName == "Iris"){
		$("#irisPeriodForm").show();
		$("#csvPeriodForm").hide();
	}
	else{
		$("#irisPeriodForm").hide();
		$("#csvPeriodForm").show();
	}
	
	if (deoPreferenceOpt1 == ""){
		$("#downloadContainer").hide();
		$("#sendContainer").hide();
	}else if (deoPreferenceOpt1 == "D"){
		$("#downloadContainer").show();
		$("#sendContainer").hide();
	}else{
		$("#sendContainer").show();
		$("#downloadContainer").hide();
	}
	
	if (ulPreferenceOpt1 == ""){
		$("#uploadPerFileContainer").show();
		$("#uploadCSVContainer").hide();
		$("#generateContainer").show();
		$("#mappingContainer").hide();
		$("#customerSearchScreen").hide();
		
	}else if (ulPreferenceOpt1 == "C"){
		$("#uploadPerFileContainer").hide();
		$("#generateContainer").hide();
		$("#uploadCSVContainer").show();
		$("#mappingContainer").show();
		$("#generateOptionContainer").hide();
		$("#downloadOptionContainer").hide();
		$("#customerSearchScreen").hide();
		if(ulPreferenceOpt2 != null && ulPreferenceOpt2 != "" && ulPreferenceOpt2 =="T"){
			if(clientName == "Iris"){
				//$("#generateOptionContainer").show();
				$("#downloadOptionContainer").show();
				//$("#tikkingenDownloadContainer").hide();
				//$("#lonDownloadContainer").hide();
			}else{
				$("#generateOptionContainer").show();
				$("#downloadOptionContainer").show();
				$("#tikkingenDownloadContainer").hide();
				$("#lonDownloadContainer").hide();
			}
			
		}
	}else{
		$("#generateContainer").show();
		$("#uploadPerFileContainer").show();
		$("#uploadCSVContainer").hide();
		$("#mappingContainer").hide();
		$("#customerSearchScreen").hide();
	}
	if(templateVersion == "true"){
		$("#googleDrive").hide();
		$("#uploadPerFileContainer").hide();
		$("#uploadCSVContainer").hide();
		$("#generateContainer").hide();
		$("#mappingContainer").hide();
		$("#generateNewClientContainer").hide();
		$("#uploadOmniPrintContainer").show();
		$("#timeEntryScreen").hide();
		$("#customerSearchScreen").show();	
	}
	
	
}

function irisPeriod_callback_function($el, value, callback) {
    callback({
      value: value,
      valid: checkIrisPeriod(value),
      message: invalidPeriod
    });
}

function checkIrisPeriod(yearMonth) {
	
	if(yearMonth.length != 6)
		return false;
	var year = parseInt(yearMonth.substring(0,4));
	var month = parseInt(yearMonth.substring(4,6));
	
	if(year < 2010 || year >2099 || isNaN(year)) {
		$(".csvPeriod-help-block").text(invalidPeriod);
		$(".csvPeriod-help-block").show();
		$("#irisPeriod").focus();
		return false;
	}
		
	if(month < 1 || month > 12 || isNaN(month)) {
		$(".csvPeriod-help-block").text(invalidPeriod);
		$(".csvPeriod-help-block").show();
		$("#irisPeriod").focus();
		return false;
	}
	
	$(".csvPeriod-help-block").hide();
	return true;
}

function csvPeriod_callback_function($el, value, callback) {
    callback({
      value: value,
      valid: checkCsvPeriod(value),
      message: invalidPeriod
    });
}

function checkCsvPeriod(yearMonth) {
	
	if(yearMonth.length != 8)
		return false;
	var year = parseInt(yearMonth.substring(0,4));
	var month = parseInt(yearMonth.substring(4,6));
	var week = parseInt(yearMonth.substring(6,8));
	
	
	if(year < 2010 || year >2099 || isNaN(year)) {
		$(".csvPeriod-help-block").text(invalidPeriod);
		$(".csvPeriod-help-block").show();
		$("#csvPeriod").focus();
		return false;
	}
		
	if(month < 1 || month > 12 || isNaN(month)) {
		$(".csvPeriod-help-block").text(invalidPeriod);
		$(".csvPeriod-help-block").show();
		$("#csvPeriod").focus();
		return false;
	}
	
	if(isNaN(week)){
		$(".csvPeriod-help-block").text(invalidPeriod);
		$(".csvPeriod-help-block").show();
		$("#csvPeriod").focus();
		return false;
	}
	$(".csvPeriod-help-block").hide();
	return true;
}

function period_callback_function($el, value, callback) {
    callback({
      value: value,
      valid: checkPeriod(value),
      message: invalidPeriod
    });
}

function checkPeriod(yearMonth) {
	
	if(yearMonth.length != 6)
		return false;
	var year = parseInt(yearMonth.substring(0,4));
	var month = parseInt(yearMonth.substring(4,6));
	
	if(year < 2010 || year >2099) {
		$(".period-help-block").text(invalidPeriod);
		$(".period-help-block").show();
		$("#period").focus();
		return false;
	}
		
	if(month < 1 || month > 12) {
		$(".period-help-block").text(invalidPeriod);
		$(".period-help-block").show();
		$("#period").focus();
		return false;
	}
	$(".period-help-block").hide();
	return true;
}

function mapPeriod_callback_function($el, value, callback) {
    callback({
      value: value,
      valid: checkMapPeriod(value),
      message: invalidPeriod
    });
}

function checkMapPeriod(yearMonth) {
	
	if(yearMonth.length != 6)
		return false;
	var year = parseInt(yearMonth.substring(0,4));
	var month = parseInt(yearMonth.substring(4,6));
	
	if(year < 2010 || year >2099) {
		$(".period-help-block").text(invalidPeriod);
		$(".period-help-block").show();
		$("#mapPeriod").focus();
		return false;
	}
		
	if(month < 1 || month > 12) {
		$(".period-help-block").text(invalidPeriod);
		$(".period-help-block").show();
		$("#mapPeriod").focus();
		return false;
	}
	$(".period-help-block").hide();
	return true;
}

function hoofdfirma_callback_function($el, value, callback) {
    callback({
      value: value,
      valid: checkHoofdfirmaPeriod(value),
      message: invalidPeriod
    });
}

function checkHoofdfirmaPeriod(hoofdfirma) {
	
	if(Number.isInteger(hoofdfirma)) {
		
		return true;
	}
		
	return false;
}

function openDriveFolder(folderId){
	var url ="https://drive.google.com/drive/u/0/folders/"+folderId;
	var newTab = window.open('', '_blank');
	newTab.location = url;
}

function openDriveFolderForLooncode(fileId){

	var url ="https://docs.google.com/spreadsheets/d/"+fileId+"/edit#gid=0";
	var newTab = window.open('', '_blank');
	newTab.location = url;
}

function uploadPerFile() {
	var xhr = new XMLHttpRequest();
	xhr.open('POST', '/time?method=uploadPerfile', true);
	xhr.onload = function(e) {
		if (this.status == 200) {
			var obj = JSON.parse(this.responseText);
			if(obj.status == "1") {
				$("#uploadScreen").modal("hide");
				$("#uploadMsg").html(obj.error);				
			} else {
				$("#errorMsg_upload").html(obj.error);
				$("#errorMsgDiv_upload").show();
			}
		} else {
		
		}
	};
	
	var fileInput = document.getElementById('perFile');
	var file = fileInput.files[0];
	var formData = new FormData();
	formData.append("perFile", file);

	xhr.send(formData);
}

function uploadCsvFile() {
	var xhr = new XMLHttpRequest();
	xhr.open('POST', '/time?method=uploadCsvFile', true);
	xhr.onload = function(e) {
		if (this.status == 200) {
			var obj = JSON.parse(this.responseText);
			if(obj.status == "1") {
				$("#uploadCsvScreen").modal("hide");
				$("#uploadCsvMsg").html(obj.error);
				$("#errorMsgDiv_uploadCsv").hide();
				var input = $("#csvFile");
				input.replaceWith(input.val('').clone(true));
			} else {
				$("#errorMsg_uploadCsv").html(obj.error);
				$("#errorMsgDiv_uploadCsv").show();
			}
		} else {
		
		}
	};
	
	var csvPeriod = "";
	var clientName = $('#selectedClientNm').html();
	if(clientName == "Iris"){
		csvPeriod = $("#irisPeriod").val();
	}
	else{
		csvPeriod = $("#csvPeriod").val();
	}
	
	var fileInput = document.getElementById('csvFile');
	var file = fileInput.files[0];
	var formData = new FormData();
	formData.append("csvPeriod", csvPeriod);
	formData.append("csvFile", file);
	

	xhr.send(formData);
}

function openPeriodScreen() {
	$("#errorMsgDiv").hide();
	
	$.ajax
   ({
	   cache: false,
       type: "GET",
       url: "/time",
       dataType: 'json',
	   data: {
			   method : 'retrievePerfile'
		   },
       async: true,
       success: function (response){
			   buildPeriodList(response);
       },error: function(response){
         // globalErrorMethod(response);
       }
    })
}
function openTemplateScreen() {
	$("#errorMsgDiv").hide();
	
	$.ajax
   ({
	   cache: false,
       type: "GET",
       url: "/time",
       dataType: 'json',
	   data: {
			   method : 'retrieveTemplateFile',
			   dropDown : searchFrom
		   },
       async: true,
       success: function (response){
    	   if(response.length>0){
    		   buildTemplateList(response);
    	   } 
       },error: function(response){
    	 
       }
    })
}

function openProcessScreen() {
	$("#errorMsgDiv_process").hide();
	$("#logMsgDiv_process").hide();
	$("#processCalendarLoading").show();
	$('#calendar').empty();
	$('#calendar').prepend($("<option></option>").html("Loading...."));
	var filter = ulPreferenceOpt1 == "C" ? "none" : "calendar";
	$.ajax
	({
		cache: false,
		type: "GET",
		url: "/time",
		dataType: 'json',
		data: { method : 'retrieveCalendar',retrieveFilter : filter },
		async: true,
		success: function (response)  {
			if (response) {
				$('#calendar').empty();
				$.each(response, function (i) {
					var name = response[i].title;
					var docID = response[i].docId;
					if(name)
						$('#calendar')
						    .append($("<option></option>")
						    .attr("value",docID)
						    .text(name)); 
				});
				
				$("#calendar").html($('#calendar option').sort(function(x, y) {
		            return $(x).text() < $(y).text() ? -1 : 1;
		        }));

		        $("#calendar").get(0).selectedIndex = 0;	
			}
			$("#processCalendarLoading").hide();
		},error: function(response){
			//globalErrorMethod(response);
			$("#processCalendarLoading").hide();
		}
	});
}

function openSendScreen() {
	$("#errorMsgDiv_send").hide();
	
	$.ajax
	({
		cache: false,
		type: "GET",
		url: "/time",
		dataType: 'json',
		data: { 
			method : 'retrieveLon' },
		async: true,
		success: function (response)  {
			if (response) {
				$('#lon').empty();
				$.each(response, function (i) {
					var name = response[i].title;
					var docID = response[i].docId;
					if(name)
						$('#lon')
						    .append($("<option></option>")
						    .attr("value",docID)
						    .text(name)); 
				});
				
				$("#lon").html($('#lon option').sort(function(x, y) {
		            return $(x).text() < $(y).text() ? -1 : 1;
		        }));

		        $("#lon").get(0).selectedIndex = 0;		
			}
			
		},error: function(response){
			globalErrorMethod(response);
		}
	});
}

function openDownloadScreen() {
	$("#errorMsgDiv_download").hide();
		var value = $('#downloadOption').val() || "";
		if(value == "t"){
			$("#tikkingenDownloadContainer").show();
			$("#lonDownloadContainer").hide();
			$.ajax
			({
				cache: false,
				type: "GET",
				url: "/time",
				dataType: 'json',
				data: { 
					method : 'retrieveTikkingen' },
				async: true,
				success: function (response)  {
					if (response) {
						$('#tikkingenDownload').empty();
						$.each(response, function (i) {
							var name = response[i].title;
							var docID = response[i].docId;
							if(name)
								$('#tikkingenDownload')
								    .append($("<option></option>")
								    .attr("value",docID)
								    .text(name)); 
						});
						
						$("#tikkingenDownload").html($('#tikkingenDownload option').sort(function(x, y) {
				            return $(x).text() < $(y).text() ? -1 : 1;
				        }));

				        $("#tikkingenDownload").get(0).selectedIndex = 0;		
					}
				},error: function(response){
					globalErrorMethod(response);
				}
			});
			
		}
	$.ajax
	({
		cache: false,
		type: "GET",
		url: "/time",
		dataType: 'json',
		data: { 
			method : 'retrieveLon' },
		async: true,
		success: function (response)  {
			if (response) {
				$('#lonDownload').empty();
				$.each(response, function (i) {
					var name = response[i].title;
					var docID = response[i].docId;
					if(name)
						$('#lonDownload')
						    .append($("<option></option>")
						    .attr("value",docID)
						    .text(name)); 
				});
				
				$("#lonDownload").html($('#lonDownload option').sort(function(x, y) {
		            return $(x).text() < $(y).text() ? -1 : 1;
		        }));

		        $("#lonDownload").get(0).selectedIndex = 0;		
			}
			
		},error: function(response){
			globalErrorMethod(response);
		}
	});
}

function openDownloadLONScreen() {
	$("#errorMsgDiv_download").hide();
	$.ajax
	({
		cache: false,
		type: "GET",
		url: "/time",
		dataType: 'json',
		data: { 
			method : 'retrieveTemplateLon',
			dropDown : searchFrom},
		async: true,
		success: function (response)  {
			if (response) {
			
					$('#lonTemplateDownload').empty();
					
					$.each(response, function (i) {
						var name = response[i].title;
						var docID = response[i].docId;
						if(name)
							$('#lonTemplateDownload')
							    .append($("<option></option>")
							    .attr("value",docID)
							    .text(name)); 
					});
					
					$("#lonTemplateDownload").html($('#lonTemplateDownload option').sort(function(x, y) {
			            return $(x).text() < $(y).text() ? -1 : 1;
			        }));

			        $("#lonTemplateDownload").get(0).selectedIndex = 0;	
				
			}else{
				 $("#downloadLONScreen").modal("hide");
				  $("#downloadLonFilePrintMsg").html("No LON generate yet!");
			}
			
		},error: function(response){
			
			  $("#downloadLONScreen").modal("hide");
			  $("#downloadLonFilePrintMsg").html("No LON generate yet!");
		
		}
	});
}

function openDownloadOmniScreen() {
	$("#errorMsgDiv_download").hide();
	$.ajax
	({
		cache: false,
		type: "GET",
		url: "/time",
		dataType: 'json',
		data: { 
			method : 'retrieveTemplate',
			dropDown : searchFrom},
		async: true,
		success: function (response)  {
			if (response) {
			
					$('#omniTemplateDownload').empty();
					
					$.each(response, function (i) {
						var name = response[i].title;
						var docID = response[i].docId;
						if(name)
							$('#omniTemplateDownload')
							    .append($("<option></option>")
							    .attr("value",docID)
							    .text(name)); 
					});
					
					$("#omniTemplateDownload").html($('#omniTemplateDownload option').sort(function(x, y) {
			            return $(x).text() < $(y).text() ? -1 : 1;
			        }));

			        $("#omniTemplateDownload").get(0).selectedIndex = 0;	
				
			}else{
				 $("#downloadOmniScreen").modal("hide");
				  $("#downloadOmniFilePrintMsg").html("No Template generate yet!");
			}
			
		},error: function(response){
			
			  $("#downloadLONScreen").modal("hide");
			  $("#downloadLonFilePrintMsg").html("No LON generate yet!");
		
		}
	});
}

function genCalendar(docID, year, month) {
	$.ajax
   ({
       type: "GET",
       url: "/time",
       dataType: 'json',
	   data: {
			   method : 'generateCalendear',
			   docID : docID,
			   year : year,
			   month : month
		   },
       async: true,
       success: function (response){
    	   if(response.status == "1") {

    		   if (response.spreadsheet != null) {
    			   runCalenderFirstLaunch(response.spreadsheet);
    			 
    			   $("#errorMsg").html("Applying validation, pls wait...<img src='img/ajax-loader2.gif' alt='Loading' />");
    		  }
    		  
		   } else {
			   $("#errorMsg").html(response.error);
			   $("#errorMsgDiv").show();
    	   }
    		   
       },error: function(response){
         // globalErrorMethod(response);
       }
    })
}

function getMyResult(json){
	
	if (json !=null){
		if (json.result == "1"){
			 $("#periodScreen").modal("hide");
		     $("#generateMsg").html(successGenCalendar);
		}else{
			  $("#errorMsg").html(failedGenCalendar);
			   $("#errorMsgDiv").show();
		}
		
	}else{
		  $("#errorMsg").html(failedGenCalendar);
		   $("#errorMsgDiv").show();
	}
	
}

function runCalenderFirstLaunch(spreadsheetID){

	$.ajax
	   ({
	       type: "GET",
	       url: "https://script.google.com/macros/s/AKfycbz-zR4pa9PvJ2n2Ogebgj6fwN74EGCu9DFYYfHKijj0LD9z1-1X/exec?prefix=getMyResult",
	       dataType: 'jsonp',
	       crossDomain: true,
	       contentType: "application/json",
	       
		   data: {
			   	spreadsheetid : spreadsheetID
		   },
	       async: true,
	       success: function (response){

	    	   /*
	    	   $.each( response, function( key, val ) {
	    		   var values = val;
	    		   var keys = key;
	    		  });
	    		  */
	    	   if(response.result == "1") {

			   } else {

	    	   }
	    		   
	       },error: function(response){

	          //globalErrorMethod(response);
	       }
	    })
	
}

function genTikkingen(docID,folderNm) {
	$.ajax
   ({
       type: "GET",
       url: "/time",
       dataType: 'json',
	   data: {
			   method : 'processTikkingen',
			   docID : docID,
			   folderNm : folderNm,
		   },
       async: true,
       success: function (response){
    	   if(response.status == "1") {
			   $("#processScreen").modal("hide");
			   $("#processMsg").html(response.error);
		   } else {
			   $("#errorMsg_process").html(response.error);
			   $("#errorMsgDiv_process").show();
    	   }
    		   
       },error: function(response){
          //globalErrorMethod(response);
       }
    })
}

  
function genLON(docID,folderNm, type) {
	$.ajax
   ({
       type: "GET",
       url: "/time",
       dataType: 'json',
	   data: {
			   method : 'processCalendar',
			   docID : docID,
			   folderNm : folderNm,
			   type : type
		   },
       async: true,
       success: function (response){
    	   if(response.status == "1") {
    		   if(type == "validate"){
    			   $("#processLogTable tbody tr").empty();
    			   var logData = JSON.parse(response.logData);
    			   $("#errorMsgDiv_process").hide();
    			   var returnMsg = response.error;
    			   if(logData != null && logData.length > 0){
    				   var logFileLink = "https://docs.google.com/a/mincko.com/spreadsheets/d/"+response.logFileID+"/edit";
    				   var logFileIcon= '<a href="'+logFileLink+'" target="_blank"><span class="glyphicon glyphicon-export"></span></a>';
    				   returnMsg = response.error + '&nbsp;'+'&nbsp;'+'&nbsp;'+ logFileIcon;
    				   
    				   var link =  "https://docs.google.com/a/mincko.com/spreadsheets/d/"+docID+"/edit";
    				   var tdLink = '<a href="'+link+'" target="_blank"><span class="glyphicon glyphicon-new-window"></span></a>';
    				   $.each(logData, function (index,val) {
    					   var newRow = '';
    					   newRow = '<tr>'+
    					   	'<td>'+val["date"]+'</td>'+	
							'<td>'+val["time"]+'</td>'+
							'<td>'+val["msg"]+'</td>'+
							'<td>'+tdLink+'</td>'+
							'</tr>';
    					   $('table#processLogTable').append(newRow);
    				   });
    			   }else{
    				   var newRow = '<tr>'+
					   	'<td colspan="4">'+response.emptyLog+'</td>'+
						'</tr>';
    				   $('table#processLogTable').append(newRow);
    			   }
    			   
    			   $("#logMsg_process").html(returnMsg);
    			   $("#logMsgDiv_process").show();
    		   }else{
    			   $("#processScreen").modal("hide");
    			   $("#processMsg").html(response.error);
    		   }
    		 
		   } else {
			   $("#errorMsg_process").html(response.error);
			   $("#errorMsgDiv_process").show();
    	   }
    		   
       },error: function(response){
          //globalErrorMethod(response);
       }
    })
}

function sendLON(docID, emailAddress) {
	
    $.ajax
    ({
        type: "GET",
        url: "/time",
        dataType: 'json',
        data: {
                method : 'sendLON',
                docID : docID,
                email : emailAddress
            },
        async: true,
        success: function (response){
            if(response.status == "1") {
                $("#sendScreen").modal("hide");
                $("#sendMsg").html(response.error);
            } else {
                $("#errorMsg_send").html(response.error);
                $("#errorMsgDiv_send").show();
            }
                
        },error: function(response){
           //globalErrorMethod(response);
        }
     })

}

function downloadLON(docID,spreadsheet, templateClient) {
	$.ajax
	({
	   cache: false,
       type: "GET",
       url: "/time",
       dataType: 'json',
	   data: {
			   method : 'downloadLON',
			   docID : docID,
			   spreadsheet:spreadsheet,
			   templateClient:templateClient
		   },
       async: true,
       success: function (response){
    	 //debugger;
    	   if(response.status == "1") {
    		  $(this).attr('download', 'ExportToExcel.xls') // set file name (you want to put formatted date here)
               .attr('href', response.fileLink)                     // data to download
               .attr('target', '_blank');
    		  
    		   //downloadFrame.location = response.fileLink;
    			window.location.assign(response.fileLink);
    		  	//window.location.assign(response.fileLink);
    		   $("#downloadScreen").modal("hide");
    		   $("#downloadLONScreen").modal("hide");		   
    		   $("#downloadMsg").html(response.error);
		   } else {
			   $("#errorMsg_download").html("error");
			   $("#errorMsgDiv_download").show();
    	   }
    		   
       },error: function(response){
    	  // alert('1129')
          //globalErrorMethod(response);
       }
    })
}

function buildPeriodList(response) {
	if (response) {
		$('#perfile').empty();
		$.each(response, function (i) {
			var name = response[i].title.replace('.TXT', '');
			var docID = response[i].docId;
			if(name)
				$('#perfile')
				    .append($("<option></option>")
				    .attr("value",docID)
				    .text(name)); 
		});
		
		$("#perfile").html($('#perfile option').sort(function(x, y) {
            return $(x).text() < $(y).text() ? -1 : 1;
        }));

        $("#perfile").get(0).selectedIndex = 0;		
	}
}
function buildTemplateList(response) {
	if (response) {
		$('#templatefile').empty();
		$.each(response, function (i) {
			var name = response[i].title.replace('.TXT', '');
			var docID = response[i].docId;
			if(name)
				$('#templatefile')
				    .append($("<option></option>")
				    .attr("value",docID)
				    .text(name)); 
		});
		
		$("#templatefile").html($('#templatefile option').sort(function(x, y) {
            return $(x).text() < $(y).text() ? -1 : 1;
        }));

        $("#templatefile").get(0).selectedIndex = 0;		
	}
}

function uploadOmniFile(year, month) {
	var inputFileImage = document.getElementById("perOmniFile");
	var file = inputFileImage.files[0];
	var data = new FormData();
	data.append("perOmniFile",file);
	$.ajax({
		
		url:   "/time?method=uploadOmniFile&fileName="+$("#fileName").val()+"&paramYear="+year+"&paramMonth="+month+"&dropDown="+searchFrom+"&looncodesFileId="+looncodeFileId,
		type:  'POST',
		cache : false,
		data : data,
		processData : false,
		contentType : false,
		dataType: "json",       
		success:  function (response) {
			if(response.status == "1") {
				  $("#uploadOmniPrintScreen").modal("hide");	
				  $("#errorMsgDiv_templateUpload").hide();
				  $("#uploadOmniPrintMsg").html(response.error);
			} else {
				   $("#errorMsg_templateUpload").html(response.error);
				   $("#errorMsgDiv_templateUpload").show();
	 	   }
	
		},error: function(response){
			 $("#errorMsg_templateUpload").html(response.error);
			 $("#errorMsgDiv_templateUpload").show();
	    }
	}); 
	
	
}
function generateTemplateLONFile(docID, year, month,correctieaangifte,generatie,typeOfLON,bedragenDoorsturen,hoofdfirma,beschrijvingLON) {
	
	$.ajax({
	url:   "/time?method=generateTemplateLONFile&docID="+docID+"&year="+year.toString()+"&month="+month.toString()+"&dropDown="+searchFrom+
	"&correctieaangifte="+correctieaangifte+"&generatie="+generatie+"&typeOfLON="+typeOfLON+"&bedragenDoorsturen="+bedragenDoorsturen
	+"&hoofdfirma="+hoofdfirma+"&beschrijvingLON="+beschrijvingLON,
	type:  'GET',
	cache : false,
	processData : false,
	contentType : false,
	dataType: "json",       
	success:  function (response) {  
		
		if(response.status == "1") {
			  $("#generateIonFileScreen").modal("hide");	
			  $("#errorMsg_templateLONUpload").hide();
			  $("#generateIonFilePrintMsg").html(response.error);
		} else {
			   $("#errorMsg_templateLONUpload").html(response.error);
			   $("#errorMsgDiv_templateLONUpload").show();
	   }
		
	},error: function(response){
			 $("#generateIonFileScreen").modal("hide");
			 $("#generateIonFileScreen").hide();
			 $("#errorMsg_templateLONUpload").html("");
			/* window.location.assign("https://docs.google.com/a/mincko.com/uc?id=0B9Z5cXLo9qpsRk9yUDhabzlJM0U&export=download");
			 if(response.fileLink!=null) window.location.assign("https://docs.google.com/a/mincko.com/uc?id=0B9Z5cXLo9qpsRk9yUDhabzlJM0U&export=download");*/
	    }
	}); 
	
	
}

function buildNewTimeEntry(response) {
	var clientId = response.securexCustomer.id;
	$.ajax({
		cache: false,
        type: "GET",
        url: 'time',
		dataType: 'text',
	    data: {clientID: response.securexCustomer.id, status:"new" },
	
        success: function(resp, textStatus, jqXHR) {
    		$("#timeEntryScreen").hide();
    		$("#customerSearchScreen").show();	
      		
        }
    });

}
