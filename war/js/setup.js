/**
 * 
 */

$(document).ready(function() {
	initEvent();
});

function initEvent(){
	
	$("#DEO_panel_link").click();
	retrieveLONPreference();
	$('#btn-lonSubmit').on("click",function() {
		$('#lonPreferenceForm').find('input,select').not("[type=submit]").jqBootstrapValidation();
		var invalid = $('#lonPreferenceForm').find('input,select').not("[type=submit]").jqBootstrapValidation('hasErrors')
		if(invalid) return;
		saveLONPreference();
	});
}

function retrieveLONPreference() {
	$.ajax
	({
       type: "GET",
       url: "/setup",
       dataType: 'json',
	   data: {
			   method : 'retrieveLonPreference'
			  
		   },
       async: true,
       success: function (response){

    	   if(response.status == "1") {
    		   var preference = response.preference;
    		   if (preference.option1 != null && preference.option1 =="S" ){
    			   $("#downloadIonOptionS").prop("checked",true);
    	   		}else{
    	   		 $("#downloadIonOptionD").prop("checked",true);
    	   		}
    		   if (preference.id != null && preference.id != ""){ 
    			   
    			   $("#preferenceID").val(preference.id); 

    		   }

		   } else if (response.status == "2"){
			  
    	   }else{
    		   $("#errorMsgDiv_lonPreference").html("error");
			   $("#errorMsgDiv_lonPreference").show();
    	   }
    	   
    		   
       },error: function(response){
          //globalErrorMethod(response);
       }
    })
}


function saveLONPreference() {
	var securexpreferenceID = $("#preferenceID").val();

	$.ajax
	({
       type: "GET",
       url: "/setup",
       dataType: 'json',
	   data: {
			   method : 'saveLonPreference',
			   preferenceID : securexpreferenceID,
			   lonOption : $('#downloadIonOptionD').prop('checked') ? 'D': 'S'
			   
		   },
       async: true,
       success: function (response){

    	   if(response.status == "1") {
    		   $("#preferenceID").val(response.preferenceID); 
    		   $("#errorMsg_lonPreference").html(response.message);
    		   $("#errorMsgDiv_lonPreference").show();
    		  
		   } else {
			   $("#errorMsgDiv_lonPreference").html("error");
			   $("#errorMsgDiv_lonPreference").show();
    	   }
    		   
       },error: function(response){
          //globalErrorMethod(response);
       }
    })
}