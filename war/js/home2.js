$(document).ready(function() {
	initEvent();
});

var lastPage = "";
function initEvent(){
	/*$("#clientSelect").bind("change",function(){
		clientOnChange($(this).val());
	});*/
	
	$("#clientContainer").delegate("a", "click", function(e){
		clientOnChange2($(this));
	});
	
	$( "div#sub-menu" ).delegate( "a", "click", function(e){
		e.preventDefault();
		loadBodyTran($(this));
	});	
	
	if(getUrlParameter('page') != null && getUrlParameter('page') != 'null') {
		loadBodyTran($( "a[pageLink*='" + getUrlParameter('page') + "']" ), getUrlParameter('page')); 
		lastPage = getUrlParameter('page');
	}
	else {
		$("#menuUI li:first-child").children("a").click();
		//lastPage = $("#menuUI li:first-child").children("a").pageLink;
	}
}

function clientOnChange(clientValue) {
	if(clientValue == "logout")
		location.href = "/changeuser"; //move to menu
	else if(clientValue == "own")
		location.href = "/home?behalf=own"; //temp removed
	else {
		location.href = "/home?behalf="+clientValue+"&page="+lastPage;
	}
}

function clientOnChange2(clientValue) {
	var htmlLink = clientValue.attr("href");
	clientValue.attr("href","#");
	location.href = htmlLink+"&page="+lastPage;
	
	
}

function langOnChange(lang, client) {
	location.href = "/lang?lang="+lang+"&behalf="+client+"&page="+lastPage;
}

function loadBodyTran(linkObj, pageName) {
	lastPage = pageName;

	$("#menuUI li").removeClass("active");
	if(linkObj != null) {
		$(linkObj).parent().addClass("active");
	}
	
	//will be remove after menu created.
	if (pageName.toLowerCase() != "time" && pageName.toLowerCase() != "log" && pageName.toLowerCase() != "setup" ){
		
		var content = "<div class='container'>"+
						"<div class='row clearfix'>"+
						"<div class='col-md-12 column'>"+
						"<h3>"+pageName+"</h3>"+
						"</div>"+
						"</div>";
		$("#tranContent").html(content);
	
	}else{
		
		$.ajax({
			cache: false,
	        type: "GET",
	        url: pageName,
			dataType: 'text',
		    data: {clientID: selectedClientId },
		
	        success: function(resp, textStatus, jqXHR) {
		    	$("#tranContent").html(resp);
		    	
		    	if (pageName.toLowerCase() == "log" ){
		    		drawLogTable();
		    	}
		    	
	            
	        }
	    });
    }
	
}

function drawLogTable(){
	
	


}


function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
    return null;
}

function buildNewTimeEntry(response) {
	var clientId = response.securexCustomer.id;
	$.ajax({
		cache: false,
        type: "GET",
        url: 'time',
		dataType: 'text',
	    data: {clientID: response.securexCustomer.id, status:"new" },
	
        success: function(resp, textStatus, jqXHR) {
    		$("#timeEntryScreen").hide();
    		$("#customerSearchScreen").show();	
      		$('#btn-newMappingSubmit').on("click",function() {
      			uploadOmniFile()
    			//genMappingFile($("#fileName").val());
    			
    			
    		});
      		$('#btn-newUploadSubmit').on("click",function() {
      			uploadOmniFile()
    			//genMappingFile($("#fileName").val());
    			
    			
    		});
	    
        }
    });
/* var content = ' <script src="/js/time.js"></script><div class="row-fluid clearfix">'+
				 '<div class="col-md-12 column marginBtm50">'+
				 '<div class="row-fluid clearfix">'+
					'<div class="col-md-3 column marginTop10" id="mappingContainer">
						'<div><a target="_blank" href="<%=request.getAttribute("MappingFile")%>"> <img src="../img/people_360.png" class="img-icon" ><%=TranslationService.translate(secUser.getPreflanguage(),"time","mappingCSV")%></a></div>
					
					'</div>

					'<div id="generateContainer" class="col-md-3 column marginTop10">'+
						'<div><a data-toggle="modal" id="btn-generate" data-target="#periodScreen"><img src="../img/people_360.png" class="img-icon"  >'+generate+'</a>'+
						'<div class="label label-default generateMsg" id="generateMsg"></div>'+
						'</div>'+
					'</div>'+

				'</div>';
   $("#tranContent").html("");
	$("#tranContent").html(content);*/
}

$("#form").keydown(function(event) {
    if (event.keyCode == 13){
    	$("#ac").val();
    	$.ajax
        ({
            type: "GET",
            url: "/searchCustomer",
        	data: {
        		services:"searchCustomer", 
        		customer:$("#ac").val()
        		},
            dataType: 'json',
            async: true,
            success: function (response){
            	if(response.status == "new" ) {
            		buildNewTimeEntry(response);
            		return
            	}
            	else if(response.status == "noaccess" ) {
            		$("#tranContent").html("");
            		$("#tranContent").html("<div class=\"error\>"+noaccess+"</div>");
            		return
            	}
            	else
            		loadBodyTran($(this), "time");
            	
              },error: function(response){
            	  $("#tranContent").html("");
          		$("#tranContent").html("<div class=\"error\>"+noaccess+"</div>");
            },complete: function(response){
            	//isRunning =false;
            	//loadBodyTran($(this), "time");
            }
        }); 
    	// $("#log").append($("<li />").text("Enter key detected"+$("#ac").val()));
    }
       
});