
function downloadSpeed() {

    if (window.XMLHttpRequest) {
        xhttp = new XMLHttpRequest();
    }
    else 
    {
        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var startTime = new Date().getTime();
    xhttp.open("GET", "/data/wallpaper_image.jpg", false);
    xhttp.send("");
    
    xmlDoc = xhttp.responseXML;

    var endTime =  new Date().getTime();

    var time = (endTime - startTime) / 1000;

    var speed = Math.round(1042 / time);

    return speed;
  
}
function latencySpeed() {

    if (window.XMLHttpRequest) {
        xhttp = new XMLHttpRequest();
    }
    else // Internet Explorer 5/6
    {
        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var latencySpeed = 0;
	for(var i=0;i<5;i++){

		    var startTime = new Date().getTime();
		    xhttp.open("GET", "/data/smaller_image.jpg", false);
		    xhttp.send("");
		    xmlDoc = xhttp.responseXML;
		    var endTime = new Date().getTime();
		    var time = (endTime - startTime) / 1000;
		    latencySpeed += time;		
	}
	latencySpeed = Math.floor((latencySpeed/5) * 100) / 100;
    return latencySpeed;
   
}

function javascriptPerformance() {

    var maxRow = 50;
    var maxCol = 50;

    if (window.XMLHttpRequest) {
        xhttp = new XMLHttpRequest();
    }
    else // Internet Explorer 5/6
    {
        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xhttp.open("GET", "data/testData.xml", false);
    xhttp.send("");
    xmlDoc = xhttp.responseXML;

    var StartDate = new Date();
    var StartTime = StartDate.getTime();

    for (var index = 0; index < 1; index++) {

        var table = document.createElement("TABLE");
        for (var iRow = 0; iRow < maxRow; iRow++) {
            var row = document.createElement('TR');
            for (var iCol = 0; iCol < maxCol; iCol++) {
                var cell1 = document.createElement('TD');
                var cell2 = document.createElement('TD');
                var checkbox = document.createElement('INPUT');
                checkbox.type = 'checkbox';
                cell1.appendChild(checkbox);
                row.appendChild(cell1);
                var childNodes = xmlDoc.getElementsByTagName("childLevel1");
                var iRandom = Math.floor(Math.random() * 100);
                cell2.innerHTML = childNodes[iRandom].getAttribute("id");
                row.appendChild(cell2)
                table.appendChild(row);
            }
        }
    }

    var EndDate = new Date();
    var EndTime = EndDate.getTime();

    var time = (EndTime - StartTime) / 1;

    var score = time;
    return score;
}
