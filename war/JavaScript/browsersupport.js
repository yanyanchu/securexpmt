

function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regexS = "[\\?&]" + name + "=([^&#]*)";
	var regex = new RegExp(regexS);
	var results = regex.exec(window.location.href);
	if (results == null)
		return "";
	else
		return decodeURIComponent(results[1].replace(/\+/g, " "));
}

$("button#search").click(function() {
	var username = $('#username').val();
	getUserResults(username);
});
$(document).ready(function() {
	var username = "username"
		 username = getParameterByName('username');

	
		$("#link").append("<a href=\"http://www.google.com\" onClick=\"return websitekick('cbs', 'customer', '"+username+"');\">Open Web Application</a> ");
	

});

function getUserResults(username) {

	$.ajax({
		type : "GET",
		url : "/cron/userMonitorServlet",
		data : "getUserResults=X&username=" + username,
		success : function(response) {
			var obj = JSON.parse(response);
			var tbody = '';
			if (obj.data != null) {

				for (var i = 0; i < obj.data.length; i++) {
					var date = new Date(obj.data[i].timestamp);
					tbody += "<tr><td>" + (date.getMonth() + 1) + '/'
							+ date.getDate() + '/' + date.getFullYear()
							+ "<td>" + obj.data[i].browserName + "</td>"
							+ "<td>" + obj.data[i].browserVersion + "</td>"
							+ "<td>" + obj.data[i].platform + "</td>" + "<td>"
							+ obj.data[i].platformVersion + "</td>" + "<td>"
							+ obj.data[i].downloadSpeed + "</td>" + "<td>"
							+ obj.data[i].latency + "</td>" + "<td>"
							+ obj.data[i].webPerformance + "</td></tr>";
				}
				$("#userLists").empty();
				$("#userLists").append(tbody);
			} else {
				$("#userLists").empty();
				$("#userLists").append("<td colspan=\"8\">No results found!</td>");
			}

		},
		error : function() {
			$("#userLists").empty();
			$("#userLists").append("<td colspan=\"8\">No results found!</td>");
		}
	});
}