﻿function getJSON() {
    var url = "/cron/parseuseragentServlet";

    var jsonT = false;
    var xhr = false;

    if (window.ActiveXObject) {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }
    else {
        xhr = new XMLHttpRequest();
    }
    if (!xhr) {
        alert('XMLHttp failed to instantiate');
        return false;
    }
    xhr.open('GET', url, false);
    xhr.send(null);

    return xhr;
}

function fillbenchmark() {

    var score = javascriptPerformance();
    var element = document.getElementById("webperformance");
    element.innerHTML = score ;
    
    document.formuserdata.score1.value = score;

    var latency =  latencySpeed();
    var element2 = document.getElementById("latencyspeed");
    element2.innerHTML = latency 

    var speed = downloadSpeed();
    document.formuserdata.score2.value = speed;
    var element3 = document.getElementById("downloadspeed");
    element3.innerHTML = speed + " Kb/s";

    var param1 = gup('param1');
    var param2 = gup('param2');
    var param3 = gup('param3');
    var lang = gup('lang');

    document.formuserdata.param1.value = param1;
    document.formuserdata.param2.value = param2;
    document.formuserdata.param3.value = param3;
    
    document.formuserdata.downloadSpeed.value = speed;
    document.formuserdata.latencySpeed.value = latency;
    document.formuserdata.webPerformance.value = score;
    document.formuserdata.lang.value = lang;

    var params = $('formuserdata').serialize();
    var url = "/cron/benchmarkServlet?" + params;

    http_request = false;
    if (window.XMLHttpRequest) { 
        http_request = new XMLHttpRequest();
        if (http_request.overrideMimeType) {
            http_request.overrideMimeType('text/html');
        }
    } else if (window.ActiveXObject) { // IE
        try {
            http_request = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                http_request = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (e) { }
        }
    }
    if (!http_request) {
        alert('Cannot create XMLHTTP instance');
        return false;
    }

    http_request.open('get', url, true);
    http_request.send(null);
    if (lang == "nl"){
      document.getElementById("loader").innerHTML = "Test is uitgevoerd, u kan dit venster sluiten."
      document.getElementById("text").innerHTML = ""
      }
    else if (lang == "fr"){
 		document.getElementById("loader").innerHTML = "Le test a &eacute;t&eacute; effectu&eacute;, vous pouvez fermer cette fen&ecirc;tre."
 		document.getElementById("text").innerHTML = ""
    }
   
}

function gup(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else
        return results[1];
}

function fillResolution(json, divname) {
    var resolution = screen.width + " X " + screen.height;
    document.formuserdata.screenResolution.value = resolution;
    setTimeout(function() { fillColor(json, "colorDepth") }, 200);
}

function fillJavascript(json, divname) {
    var enabled = "Yes";
    document.formuserdata.javascriptEnabled.value = "Yes";
    setTimeout(function() { fillResolution(json, "screenResolution") }, 200);
}

function fillFlash(json, divname) {
    var version = getFlashVersion().split(',').shift();
    document.formuserdata.flashVersion.value = version;
    setTimeout(function() { fillbenchmark() }, 2000);
}

function fillColor(json, divname) {

    var colordepth = screen.colorDepth;
    document.formuserdata.colorDepth.value = colordepth;
    setTimeout(function() { fillFlash(json, "flashVersion") }, 200);
}

function fillIP(json, divname) {
    document.formuserdata.ipAddress.value = json.ip;
    setTimeout(function() { fillJavascript(json, "javascriptEnabled") }, 200);
}

function fillbrowserName(json, divname) {
    
    document.formuserdata.browserName.value = json.browserName;
    document.formuserdata.browserVersion.value = json.browserVersion;
    setTimeout(function() { fillIP(json, "ip") }, 200);
}

function fillOS(json, divname) {
  
    document.formuserdata.platform.value = json.platform;
    document.formuserdata.platformVersion.value = json.platformVersion;
    setTimeout(function() { fillbrowserName(json, "browserName") }, 200);
}

function build() {

    var lang = gup('lang');
    if (lang == "nl"){
    document.getElementById("text").innerHTML = "Performance test wordt doorgevoerd. Gelieve enkele ogenblikken geduld te oefenen. Hartelijk dank voor uw medewerking!"
    }
    else if (lang == "fr"){
    document.getElementById("text").innerHTML = "Le test de performance est en cours d&#39;ex&eacute;cution. Veuillez patienter un instant. Merci de votre collaboration!"
    }
    
    var xhr = getJSON();
    var JSONdata = eval('(' + xhr.responseText + ')');
    fillOS(JSONdata, "platform");
}

function getFlashVersion() {
    try {
        try {
            var axo = new ActiveXObject('ShockwaveFlash.ShockwaveFlash.6');
            try { axo.AllowScriptAccess = 'always'; }
            catch (e) { return '6,0,0'; }
        } catch (e) { }
        return new ActiveXObject('ShockwaveFlash.ShockwaveFlash').GetVariable('$version').replace(/\D+/g, ',').match(/^,?(.+),?$/)[1];
        // other browsers
    } catch (e) {
        try {
            if (navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin) {
                return (navigator.plugins["Shockwave Flash 2.0"] || navigator.plugins["Shockwave Flash"]).description.replace(/\D+/g, ",").match(/^,?(.+),?$/)[1];
            }
        } catch (e) { }
    }
    return '0,0,0';
}
