$("button#submit").click(function() {
	$.ajax({
		type : "POST",
		url : "/cron/userMonitorServlet",
		data : $('form.form-horizontal').serialize(),
		success : function(msg) {

			$("#loader").html("Save successfully!")
			$("#myModalHorizontal").modal('hide');
			location.reload();
		},
		error : function() {
			location.reload();
			$("#myModalHorizontal").modal('hide');
		}
	});
});

function gup(name) {
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regexS = "[\\?&]" + name + "=([^&#]*)";
	var regex = new RegExp(regexS);
	var results = regex.exec(window.location.href);
	if (results == null)
		return "";
	else
		return results[1];
}

$(document)
		.ready(
				function() {

					$
							.ajax({
								type : "GET",
								url : "/cron/userMonitorServlet",
								data : "getUserLists=X",
								success : function(response) {
									var obj = JSON.parse(response);
									var tbody = '';
									if (obj.data.length>0) {
										$("#userLists").empty();
										for (var i = 0; i < obj.data.length; i++) {
											var date = new Date(
													obj.data[i].timestamp);
											var measureModus = "On each login";
											if(obj.data[i].measureModus=="1") measureModus = "Once"
											
											tbody += "<tr><td>"
													+ (date.getMonth() + 1)
													+ '/'
													+ date.getDate()
													+ '/'
													+ date.getFullYear()
													+ "</td><td>"
													+ "<a href='resultscreen.html?username="+obj.data[i].username+"'>"+obj.data[i].username
													+ "<a></td>"
													+ "</td><td>"
													+ measureModus
													+ "</td>"
													+ "<td><button class='btn btn-default' data-id='"
													+ obj.data[i].username
													+ "' data-toggle='modal' data-target='#confirm-delete'>"
													+ "X" + "</button></td></tr>";
										}
										$("#userLists").append(tbody);
									} else {
										$("#userLists").empty();
										$("#userLists").append("<td colspan=\"3\">No results found!</td>");
									}
									
								},
								error : function() {
									// alert("failure");
								}
							});
				});

$('#confirm-delete').on('show.bs.modal', function(e) {
	var username = $(e.relatedTarget).attr('data-id');
	$("#btn-delete").click(function() {
		$.ajax({
			type : "POST",
			url : "/cron/userMonitorServlet",
			data : "deleteUser=X&username=" + username,
			success : function(msg) {
				$("#confirm").modal('hide');
				location.reload();
			},
			error : function() {
				location.reload();
				// $("#confirm").modal('hide');
				$("#confirm div").addClass('hide');
			}
		});
	});

});
function deleteUser(username) {
	$("#confirm div").removeClass('hide');
	$("button#delete").click(function() {
		$.ajax({
			type : "POST",
			url : "/cron/userMonitorServlet",
			data : "deleteUser=X&username=" + username,
			success : function(msg) {
				// $("#loader").html("Save successfully!")
				$("#confirm").modal('hide');
				location.reload();
			},
			error : function() {
				location.reload();
				// $("#confirm").modal('hide');
				$("#confirm div").addClass('hide');
			}
		});
	});

}
