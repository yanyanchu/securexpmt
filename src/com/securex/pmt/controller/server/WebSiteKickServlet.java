package com.securex.pmt.controller.server;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mincko.googleutil.GoogleUtil;
import com.securex.pmt.GinaUtil;
import com.securex.pmt.mark.Mark;
import com.securex.pmt.mark.PMF;
import com.securex.pmt.mark.ParseUserAgent;
import com.securex.pmt.mark.UserMonitor;

//import com.mincko.googleutil.GoogleUtil;

@SuppressWarnings("serial")
public class WebSiteKickServlet extends HttpServlet {
	private static final Logger _logger = Logger
			.getLogger(WebSiteKickServlet.class.getName());
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		try {
			resp.setHeader("Access-Control-Allow-Origin", "*");
			resp.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			resp.setHeader("Access-Control-Allow-Headers", "X-PINGOTHER");
			resp.setHeader("Access-Control-Max-Age", "1728000");			
			
			// get IP address
			String addr = req.getRemoteAddr();
			String lang = req.getParameter("lang");
			String debug = req.getParameter("debug"); // if debug is enabled, page will not redirect
			boolean performTest = true;
			if (lang == null)
				lang = "nl";
			String useragent = req.getHeader("User-Agent");
			ParseUserAgent p = new ParseUserAgent(useragent);

			resp.setContentType("text/javascript");
			
			performTest = verifyIfUserTest(req.getParameter("param3"));
			
			if(performTest){
				// IE Browser Check
				if (p.getBrowserName().equals("MSIE")) {

					if (p.getBrowserVersion() < 9.0f)
						resp.getOutputStream().println(createFunction(lang, "IE"));
					
					//
					//else if (p.getBrowserVersion() == 11.0f)
					//	resp.getOutputStream().println(createFunction(lang, "IE11"));
					
					else {
						String ret = generateBrowsenOKCode( debug);
						resp.getOutputStream().print(  ret);
					}
					
				}
				// Firefox Browser < 6 Check
				else if ((p.getBrowserName().equals("Firefox"))
						&& (p.getBrowserVersion() < 30.0f)) {

					resp.getOutputStream().println(createFunction(lang, "FF"));
				} 
				else if ((p.getBrowserName().equals("Edge"))
						&& (p.getBrowserVersion() < 13.0f)) {

					resp.getOutputStream().println(createFunction(lang, "Edge"));
				} 
				//else if ((p.getBrowserName().equals("Firefox"))
				//		&& (p.getBrowserVersion() == 16.0f)) {
				//
				//	resp.getOutputStream().println(createFunction(lang, "FF16"));
				//}
				else {
					String ret = generateBrowsenOKCode( debug);
					resp.getOutputStream().print(  ret);
				}

			}else{
				String ret = generateBrowsenNOKCode( debug);
				resp.getOutputStream().print(  ret);
			}
			
			resp.getOutputStream().flush();
			resp.getOutputStream().close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// GreetingServiceImpl.log = GreetingServiceImpl.log + " " +
			// e.toString();
		}
	}
	
	public String generateBrowsenNOKCode( String debug){
		
		String ret = "function websitekick( param1, param2, param3) {" 
			+ (debug == null ?  "window.location.href = 'https://www.mysecurexhrservices.eu';" : "alert('redirect not called due to debug mode');")
			+ " return true;}";
		
		return ret;
		
	}

	
	public String generateBrowsenOKCode( String debug){
		
		String ret = "function websitekick( param1, param2, param3) {" 
			+ generateBenchmarkServletHtml()
			+ generateNewBenchmarkServletHtmlCode()
			+ (debug == null ?  "window.location.href = 'https://www.mysecurexhrservices.eu';" : "alert('redirect not called due to debug mode');")
			+ " return true;}";
		
		return ret;
		
	}

	public String createFunction(String lang, String actualCase) {

		String message = "";

		if ("nl".equals(lang)) {
			
			if ("Edge".equals(actualCase))
				message = GoogleUtil
						.readGoogleDocCached("1DttUKRXUrxC5oiJjBrZzaIMEOLrc2LOFSEEoIxEq_dg");
			if ("IE".equals(actualCase))
				message = GoogleUtil
						.readGoogleDocCached("1yTe5vfEfM8tVaZ43l5LG1-CFVCauc6DtnIYqNtdlGvY");
			if ("IE11".equals(actualCase))
				message = GoogleUtil
						.readGoogleDocCached("1mviHpZaB9jsEBpBJ0wKAPpBPGHMpbEYPND8k2lbnJZ0");
			if ("FF".equals(actualCase))
				message = GoogleUtil
						.readGoogleDocCached("1Q1gT4xM3sQsWR3JxE0xcYpZ3tLXhs9Rnl8PNOjR0EMw");
			if ("FF16".equals(actualCase))
				message = GoogleUtil
						.readGoogleDocCached("18SXX86EE4f7q-576e1nJN_ET_gfZ1ZN_yKDy4H4KrXk");
		}
		if ("fr".equals(lang)) {
			if ("Edge".equals(actualCase))
				message = GoogleUtil
						.readGoogleDocCached("1sWDjMnqi140wHwOOBML3pb5sUr7jOIzNWQ59C-yjI3M");
			if ("IE".equals(actualCase))
				message = GoogleUtil
						.readGoogleDocCached("1ifi3bjxqmvqZenpUf_jIbFrUnQnAAl06h0VWpYhacwc");
			if ("IE11".equals(actualCase))
				message = GoogleUtil
						.readGoogleDocCached("1IAJIqSSmPNAZ77uyuLHunh176WqMsg8FqHBFjZPL7jQ");
			if ("FF".equals(actualCase))
				message = GoogleUtil
						.readGoogleDocCached("1AN1ZrRvcJj-oA4e0g-8pJTeewVnzKT2AUdhLueG7ppY");
			if ("FF16".equals(actualCase))
				message = GoogleUtil
						.readGoogleDocCached("1RX0za0OUXTBLxb49-lVISZ1NHg6CoDtU_Va7IKwxasM");
		}

		String str = "";
		str = "function websitekick( param1, param2, param3) {\n"
				+ generateBenchmarkServletHtml()
				+ generateNewBenchmarkServletHtmlCode()
				+ "if  (document.getElementById('websitekickmodal') != null ) return false;\n"
				+ "if ( param1 == null) {alert('Please specify param1'); return false; }\n"
				+ "if ( param2 == null) {alert('Please specify param2'); return false; }\n"
				+ "if ( param3 == null) {alert('Please specify param3'); return false; }\n"
				+ "var divTag = document.createElement('div');\n"
				+ "divTag.id = 'websitekickmodal';\n"
				+ "function hidediv(){  document.getElementById(\"websitekickmodal\").style.visibility = \"hidden\";}\n"

				+ "if(!window.ActiveXObject){\n"
				+ "		divTag.setAttribute(\"style\",\"font-family: Verdana, sans-serif; padding-left:20px; color: black; font-size:small; margin-left:auto; margin-right:auto; float:center; display:block; width:640px; border:1px solid #000000; background-color:#FFFFFF;\");\n"
				+ "} else {\n"
				+ "		divTag.style.setAttribute(\"cssText\", \"font-family: Verdana, sans-serif; padding-left:20px; color: black; font-size:small; margin-left:auto; margin-right:auto; float:center; display:block; width:640px; border:1px solid #000000; background-color:#FFFFFF;\");\n"
				+ "}\n"
				+ "divTag.innerHTML = '<br><div style=\"Float:right;\"><img src=\"/images/logo_secu.gif\"></div><div style=\"margin-left: auto; margin-right: auto;text-align: left;\">"
				+  message + "</div><br>';\n"
				//+ "divTag.style.visibility = 'hidden';\n"
				+ "document.body.appendChild(divTag);" + " return false; }\n";

		return str;
	}

	public String generateBenchmarkServletHtml(){
		String systemUrl = GinaUtil.isTestServer();
		return 
		"var screenResolution = screen.width + \" X \" + screen.height;\n" +
		"var colorDepth = screen.colorDepth;\n" +
		"var javaScriptEnabled = \"Yes\";\n" +
		//"build();"+
		"var webPerformance = javascriptPerformance();"+
		"var latencySpeed = latencySpeed();"+
		"var downloadSpeed = downloadSpeed();"+
		"var email =   getParameterByName('email');"+
		"var xmlhttp; \n" +
		"var xdr; function xdronload() { if (xdr.responseText == 'true') window.location.href = 'https://www.mysecurexhrservices.eu'; else document.getElementById('websitekickmodal').style.visibility = 'visible';  } \n" +
		" if (window.XMLHttpRequest) { \n" +
		"	xmlhttp=new XMLHttpRequest(); }\n" +
		" else \n" +
		"{ \n" +
		"	xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");\n" +
		"}\n"   +
		" if ( navigator.appName == 'Microsoft Internet Explorer')\n {" +
		"		xdr = new XDomainRequest();\n" +
		//"		xdr.open(\"POST\",\""+systemUrl+"/cron/benchmarkServlet?param1=\"+param1+\"&param2=\"+param2+\"&param3=\"+param3+\"&screenResolution=\"+screenResolution+\"&colorDepth=\"+colorDepth+\"&webPerformance=\"+webPerformance+\"&latencySpeed=\"+latencySpeed+\"&downloadSpeed=\"+downloadSpeed+\"&email=\"+email+\"&score1=0&score2=0&score3=0\",false);\n" +
		"		xdr.open(\"POST\",\"http://localhost:8888/cron/benchmarkServlet?param1=\"+param1+\"&param2=\"+param2+\"&param3=\"+param3+\"&screenResolution=\"+screenResolution+\"&colorDepth=\"+colorDepth+\"&webPerformance=\"+webPerformance+\"&latencySpeed=\"+latencySpeed+\"&downloadSpeed=\"+downloadSpeed+\"&email=\"+email+\"&score1=0&score2=0&score3=0\",false);\n" +
		"		xdr.onload = xdronload;" +
		"		xdr.send();\n" +		
		"}\n" +
		" else if ( navigator.appName == 'Netscape'){\n" +
		//"	xmlhttp.open(\"GET\",\""+systemUrl+"/cron/benchmarkServlet?param1=\"+param1+\"&param2=\"+param2+\"&param3=\"+param3+\"&screenResolution=\"+screenResolution+\"&colorDepth=\"+colorDepth+\"&webPerformance=\"+webPerformance+\"&latencySpeed=\"+latencySpeed+\"&downloadSpeed=\"+downloadSpeed+\"&email=\"+email+\"&score1=0&score2=0&score3=0\", false);\n"
		"	xmlhttp.open(\"GET\",\"http://localhost:8888/cron/benchmarkServlet?param1=\"+param1+\"&param2=\"+param2+\"&param3=\"+param3+\"&screenResolution=\"+screenResolution+\"&colorDepth=\"+colorDepth+\"&webPerformance=\"+webPerformance+\"&latencySpeed=\"+latencySpeed+\"&downloadSpeed=\"+downloadSpeed+\"&email=\"+email+\"&score1=0&score2=0&score3=0\",false);\n"
		+ "	xmlhttp.send();\n"
		+ " if (document.getElementById('websitekickmodal') != null ) document.getElementById('websitekickmodal').style.visibility = 'visible';"
		+ "}\n" + 
		"else {\n" +
		//"	xmlhttp.open(\"GET\",\""+systemUrl+"/cron/benchmarkServlet?param1=\"+param1+\"&param2=\"+param2+\"&param3=\"+param3+\"&screenResolution=\"+screenResolution+\"&colorDepth=\"+colorDepth+\"&webPerformance=\"+webPerformance+\"&latencySpeed=\"+latencySpeed+\"&downloadSpeed=\"+downloadSpeed+\"&email=\"+email+\"&score1=0&score2=0&score3=0\", true);\n"
		"xmlhttp.open(\"GET\",\"http://localhost:8888/cron/benchmarkServlet?param1=\"+param1+\"&param2=\"+param2+\"&param3=\"+param3+\"&screenResolution=\"+screenResolution+\"&colorDepth=\"+colorDepth+\"&webPerformance=\"+webPerformance+\"&latencySpeed=\"+latencySpeed+\"&downloadSpeed=\"+downloadSpeed+\"&email=\"+email+\"&score1=0&score2=0&score3=0\",false);\n"
		+ "xmlhttp.send();\n"
		+ "}";
	}
	public String generateNewBenchmarkServletHtmlCode(){
		String systemUrl = GinaUtil.isTestServer();
		String ret = 

				"function downloadSpeed() {"+

				"    if (window.XMLHttpRequest) {"+
				"        xhttp = new XMLHttpRequest();"+
				"    }"+
				"    else "+
				"    {"+
				"        xhttp = new ActiveXObject('Microsoft.XMLHTTP');"+
				"    }"+

				"    var startTime = new Date().getTime();"+
				"    xhttp.open('GET', '"+systemUrl+"/data/wallpaper_image.jpg', false);"+
				"    xhttp.send('');"+
				    
				"    xmlDoc = xhttp.responseXML;"+

				"    var endTime =  new Date().getTime();"+

				"    var time = (endTime - startTime) / 1000;"+

				"    var speed = Math.round(1042 / time);"+

				"    return speed;"+
				  
				"}"+
				"function latencySpeed() {"+

				"   if (window.XMLHttpRequest) {"+
				"        xhttp = new XMLHttpRequest();"+
				"    }"+
				"    else"+
				"    {"+
				"        xhttp = new ActiveXObject('Microsoft.XMLHTTP');"+
				"    }"+

				"    var latencySpeed = 0;"+
				"    for(var i=0;i<5;i++){"+

				"            var startTime = new Date().getTime();"+
				"            xhttp.open('GET', '"+systemUrl+"/data/smaller_image.png', false);"+
				"            xhttp.send('');"+
				"            xmlDoc = xhttp.responseXML;"+
				"            var endTime = new Date().getTime();"+
				"            var time = (endTime - startTime);"+
				"            latencySpeed += time;"+       
				"    }"+
				"    latencySpeed = Math.floor((latencySpeed/5) * 100) / 100;"+
				"    return latencySpeed;"+
				   
				"}"+

				"function javascriptPerformance() {"+

				"    var maxRow = 50;"+
				"    var maxCol = 50;"+

				"    if (window.XMLHttpRequest) {"+
				"        xhttp = new XMLHttpRequest();"+
				"    }"+
				"    else"+
				"    {"+
				"        xhttp = new ActiveXObject('Microsoft.XMLHTTP');"+
				"    }"+
				"    xhttp.open('GET', '"+systemUrl+"/data/testData.xml', false);"+
				"    xhttp.send('');"+
				"    xmlDoc = xhttp.responseXML;"+

				"    var StartDate = new Date();"+
				"    var StartTime = StartDate.getTime();"+

				"    for (var index = 0; index < 1; index++) {"+

				"        var table = document.createElement('TABLE');"+
				"        for (var iRow = 0; iRow < maxRow; iRow++) {"+
				"            var row = document.createElement('TR');"+
				"            for (var iCol = 0; iCol < maxCol; iCol++) {"+
				"                var cell1 = document.createElement('TD');"+
				"                var cell2 = document.createElement('TD');"+
				"                var checkbox = document.createElement('INPUT');"+
				"                checkbox.type = 'checkbox';"+
				"                cell1.appendChild(checkbox);"+
				"                row.appendChild(cell1);"+
				"                var childNodes = xmlDoc.getElementsByTagName('childLevel1');"+
				"                var iRandom = Math.floor(Math.random() * 100);"+
				"                cell2.innerHTML = childNodes[iRandom].getAttribute('id');"+
				"                row.appendChild(cell2);"+
				"                table.appendChild(row);"+
				"            }"+
				"        }"+
				"    }"+

				"    var EndDate = new Date();"+
				"    var EndTime = EndDate.getTime();"+

				"    var time = (EndTime - StartTime) / 1;"+

				"    var score = time;"+
				"    return score;"+
				"}";

		
		return ret;
		
	}
	

	public boolean verifyIfUserTest(String username){
		String condStr = "";
		Map params = new HashMap();
		PersistenceManager pm;
		pm = PMF.get().getPersistenceManager();
		Query q = pm.newQuery(com.securex.pmt.mark.UserMonitor.class);
		q.setFilter("username == emailParam");
		q.declareParameters("String emailParam");

		try {
		  List<UserMonitor> results = (List<UserMonitor>) q.execute(username);
		  if (!results.isEmpty()) {
		    for (UserMonitor p : results) {
		    	if(p.isMeasureModus().equalsIgnoreCase("1")){//measure once
		    		PersistenceManager pmMeasure;
		    		pmMeasure = PMF.get().getPersistenceManager();
					Query qMeasure = pm.newQuery(com.securex.pmt.mark.Mark.class);
					qMeasure.setFilter("username == emailParam");
					qMeasure.declareParameters("String emailParam");

					try {
					  List<Mark> resultsMark = (List<Mark>) q.execute(username);
					  if (!resultsMark.isEmpty()) {
						 return false;
					  
					  } else {
						  return true;
					  }
					} finally {
					  q.closeAll();
					}
		    		
		    		
		    	}else{// measure everytime
		    		return true;
		    		
		    	}
		      // Process result p
		    }
		  } else {
			  return true;
		  }
		} finally {
		  q.closeAll();
		}
		
		return false;
		
	}
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}
}
