package com.securex.pmt.controller.server;

import java.io.IOException;
import java.util.Calendar;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mincko.googleutil.GoogleUtil;
import com.securex.pmt.GinaUtil;
import com.securex.pmt.mark.Mark;
import com.securex.pmt.mark.PMF;
import com.securex.pmt.mark.ParseUserAgent;

//import com.mincko.googleutil.GoogleUtil;

@SuppressWarnings("serial")
public class NewWebSiteKickServlet extends HttpServlet {
	private static final Logger _logger = Logger
			.getLogger(NewWebSiteKickServlet.class.getName());
	private static String systemUrl = GinaUtil.isTestServer();
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		try {
			resp.setHeader("Access-Control-Allow-Origin", "*");
			resp.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
			resp.setHeader("Access-Control-Allow-Headers", "X-PINGOTHER");
			resp.setHeader("Access-Control-Max-Age", "1728000");			
			
			// get IP address
			String addr = req.getRemoteAddr();
			String lang = req.getParameter("lang");
			String debug = req.getParameter("debug"); // if debug is enabled, page will not redirect
			if (lang == null)
				lang = "nl";
			String useragent = req.getHeader("User-Agent");
			ParseUserAgent p = new ParseUserAgent(useragent);

			resp.setContentType("text/javascript");

			// IE Browser Check
			if (p.getBrowserName().equals("MSIE")) {

				if (p.getBrowserVersion() < 9.0f)
					resp.getOutputStream().println(createFunction(lang, "IE"));
				
				//
				//else if (p.getBrowserVersion() == 11.0f)
				//	resp.getOutputStream().println(createFunction(lang, "IE11"));
				
				else {
					String ret = generateBrowsenOKCode( debug);
					resp.getOutputStream().print(  ret);
				}
				
			}
			// Firefox Browser < 6 Check
			else if ((p.getBrowserName().equals("Firefox"))
					&& (p.getBrowserVersion() < 30.0f)) {

				resp.getOutputStream().println(createFunction(lang, "FF"));
			} 
			else if ((p.getBrowserName().equals("Edge"))
					&& (p.getBrowserVersion() < 13.0f)) {

				resp.getOutputStream().println(createFunction(lang, "Edge"));
			} 
			//else if ((p.getBrowserName().equals("Firefox"))
			//		&& (p.getBrowserVersion() == 16.0f)) {
			//
			//	resp.getOutputStream().println(createFunction(lang, "FF16"));
			//}
			else {
				String ret = generateBrowsenOKCode( debug);
				resp.getOutputStream().print(  ret);
			}

			resp.getOutputStream().flush();
			resp.getOutputStream().close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// GreetingServiceImpl.log = GreetingServiceImpl.log + " " +
			// e.toString();
		}
	}
	
	public String generateBrowsenOKCode( String debug){
		
		String ret = "function websitekick( param1, param2, param3) {" 
			+ generateBenchmarkServletHtml()
			+ generateNewBenchmarkServletHtmlCode()
			+ (debug == null ?  "window.location.href = 'https://www.mysecurexhrservices.eu';" : "alert('redirect not called due to debug mode');")
			+ " return true;}";
		
		return ret;
		
	}

	public String createFunction(String lang, String actualCase) {

		String message = "";

		if ("nl".equals(lang)) {
			
			if ("Edge".equals(actualCase))
				message = GoogleUtil
						.readGoogleDocCached("1DttUKRXUrxC5oiJjBrZzaIMEOLrc2LOFSEEoIxEq_dg");
			if ("IE".equals(actualCase))
				message = GoogleUtil
						.readGoogleDocCached("1yTe5vfEfM8tVaZ43l5LG1-CFVCauc6DtnIYqNtdlGvY");
			if ("IE11".equals(actualCase))
				message = GoogleUtil
						.readGoogleDocCached("1mviHpZaB9jsEBpBJ0wKAPpBPGHMpbEYPND8k2lbnJZ0");
			if ("FF".equals(actualCase))
				message = GoogleUtil
						.readGoogleDocCached("1Q1gT4xM3sQsWR3JxE0xcYpZ3tLXhs9Rnl8PNOjR0EMw");
			if ("FF16".equals(actualCase))
				message = GoogleUtil
						.readGoogleDocCached("18SXX86EE4f7q-576e1nJN_ET_gfZ1ZN_yKDy4H4KrXk");
		}
		if ("fr".equals(lang)) {
			if ("Edge".equals(actualCase))
				message = GoogleUtil
						.readGoogleDocCached("1sWDjMnqi140wHwOOBML3pb5sUr7jOIzNWQ59C-yjI3M");
			if ("IE".equals(actualCase))
				message = GoogleUtil
						.readGoogleDocCached("1ifi3bjxqmvqZenpUf_jIbFrUnQnAAl06h0VWpYhacwc");
			if ("IE11".equals(actualCase))
				message = GoogleUtil
						.readGoogleDocCached("1IAJIqSSmPNAZ77uyuLHunh176WqMsg8FqHBFjZPL7jQ");
			if ("FF".equals(actualCase))
				message = GoogleUtil
						.readGoogleDocCached("1AN1ZrRvcJj-oA4e0g-8pJTeewVnzKT2AUdhLueG7ppY");
			if ("FF16".equals(actualCase))
				message = GoogleUtil
						.readGoogleDocCached("1RX0za0OUXTBLxb49-lVISZ1NHg6CoDtU_Va7IKwxasM");
		}

		String str = "";
		str = "function websitekick( param1, param2, param3) {\n"
				+ generateBenchmarkServletHtml()
				+ generateNewBenchmarkServletHtmlCode()
				+ "if  (document.getElementById('websitekickmodal') != null ) return false;\n"
				+ "if ( param1 == null) {alert('Please specify param1'); return false; }\n"
				+ "if ( param2 == null) {alert('Please specify param2'); return false; }\n"
				+ "if ( param3 == null) {alert('Please specify param3'); return false; }\n"
				+ "var divTag = document.createElement('div');\n"
				+ "divTag.id = 'websitekickmodal';\n"
				+ "function hidediv(){  document.getElementById(\"websitekickmodal\").style.visibility = \"hidden\";}\n"

				+ "if(!window.ActiveXObject){\n"
				+ "		divTag.setAttribute(\"style\",\"font-family: Verdana, sans-serif; padding-left:20px; color: black; font-size:small; margin-left:auto; margin-right:auto; float:center; display:block; width:640px; border:1px solid #000000; background-color:#FFFFFF;\");\n"
				+ "} else {\n"
				+ "		divTag.style.setAttribute(\"cssText\", \"font-family: Verdana, sans-serif; padding-left:20px; color: black; font-size:small; margin-left:auto; margin-right:auto; float:center; display:block; width:640px; border:1px solid #000000; background-color:#FFFFFF;\");\n"
				+ "}\n"
				+ "divTag.innerHTML = '<br><div style=\"Float:right;\"><img src=\"/images/logo_secu.gif\"></div><div style=\"margin-left: auto; margin-right: auto;text-align: left;\">"
				+  message + "</div><br>';\n"
				//+ "divTag.style.visibility = 'hidden';\n"
				+ "document.body.appendChild(divTag);" + " return false; }\n";

		return str;
	}

	public String generateBenchmarkServletHtml(){
//	return "";
		return 
		"var screenResolution = screen.width + \" X \" + screen.height;\n" +
		"var colorDepth = screen.colorDepth;\n" +
		"var javaScriptEnabled = \"Yes\";\n" +
		//"build();"+
		"var webPerformance = javascriptPerformance();"+
		"var latencySpeed = latencySpeed();"+
		"var downloadSpeed = downloadSpeed();"+
		"var xmlhttp; \n" +
		"var xdr; function xdronload() { if (xdr.responseText == 'true') window.location.href = 'https://www.mysecurexhrservices.eu'; else document.getElementById('websitekickmodal').style.visibility = 'visible';  } \n" +
		" if (window.XMLHttpRequest) { \n" +
		"	xmlhttp=new XMLHttpRequest(); }\n" +
		" else \n" +
		"{ \n" +
		"	xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");\n" +
		"}\n"   +
		" if ( navigator.appName == 'Microsoft Internet Explorer')\n {" +
		"		xdr = new XDomainRequest();\n" +
		"		xdr.open(\"POST\",\'"+systemUrl+"'/cron/benchmarkServlet?param1=\"+param1+\"&param2=\"+param2+\"&param3=\"+param3+\"&screenResolution=\"+screenResolution+\"&colorDepth=\"+colorDepth+\"&webPerformance=\"+webPerformance+\"&latencySpeed=\"+latencySpeed+\"&downloadSpeed=\"+downloadSpeed+\"&score1=0&score2=0&score3=0\",false);\n" +
		//"		xdr.open(\"POST\",\"http://localhost:8888/cron/benchmarkServlet?param1=\"+param1+\"&param2=\"+param2+\"&param3=\"+param3+\"&screenResolution=\"+screenResolution+\"&colorDepth=\"+colorDepth+\"&score1=0&score2=0&score3=0\",false);\n" +
		"		xdr.onload = xdronload;" +
		"		xdr.send();\n" +		
		"}\n" +
		" else if ( navigator.appName == 'Netscape'){\n" +
		"	xmlhttp.open(\"GET\",\'"+systemUrl+"'/cron/benchmarkServlet?param1=\"+param1+\"&param2=\"+param2+\"&param3=\"+param3+\"&screenResolution=\"+screenResolution+\"&colorDepth=\"+colorDepth+\"&webPerformance=\"+webPerformance+\"&latencySpeed=\"+latencySpeed+\"&downloadSpeed=\"+downloadSpeed+\"&score1=0&score2=0&score3=0\", false);\n"
		//"	xmlhttp.open(\"GET\",\"http://localhost:8888/cron/benchmarkServlet?param1=\"+param1+\"&param2=\"+param2+\"&param3=\"+param3+\"&screenResolution=\"+screenResolution+\"&colorDepth=\"+colorDepth+\"&score1=0&score2=0&score3=0\",false);\n"
		+ "	xmlhttp.send();\n"
		+ " if (document.getElementById('websitekickmodal') != null ) document.getElementById('websitekickmodal').style.visibility = 'visible';"
		+ "}\n" + 
		"else {\n" +
		"	xmlhttp.open(\"GET\",\'"+systemUrl+"'/cron/benchmarkServlet?param1=\"+param1+\"&param2=\"+param2+\"&param3=\"+param3+\"&screenResolution=\"+screenResolution+\"&colorDepth=\"+colorDepth+\"&webPerformance=\"+webPerformance+\"&latencySpeed=\"+latencySpeed+\"&downloadSpeed=\"+downloadSpeed+\"&score1=0&score2=0&score3=0\", true);\n"
		//"xmlhttp.open(\"GET\",\"http://localhost:8888/cron/benchmarkServlet?param1=\"+param1+\"&param2=\"+param2+\"&param3=\"+param3+\"&screenResolution=\"+screenResolution+\"&colorDepth=\"+colorDepth+\"&score1=0&score2=0&score3=0\",false);\n"
		+ "xmlhttp.send();\n"
		+ "}";
	}
	public String generateNewBenchmarkServletHtmlCode(){
		
		String ret = /*"function getJSON() {"+
				"    var url = '/cron/parseuseragentServlet';"+
				"    var jsonT = false;"+
				"    var xhr = false;"+
				"    if (window.ActiveXObject) {"+
				"        xhr = new ActiveXObject('Microsoft.XMLHTTP');"+
				"    }"+
				"    else {"+
				"        xhr = new XMLHttpRequest();"+
				"    }"+
				"    if (!xhr) {"+
				"        alert('XMLHttp failed to instantiate');"+
				"        return false;"+
				"    }"+
				"    xhr.open('GET', url, false);"+
				"    xhr.send(null);"+
				
				"    return xhr;"+
				"}"+

				"function fillbenchmark() {"+
				"    var score = javascriptPerformance();"+
				"    var element = document.getElementById('webperformance');"+
				"    element.innerHTML = score ;"+
				    
				"    document.formuserdata.score1.value = score;"+

				"    var latency =  latencySpeed();"+
				"    var element2 = document.getElementById('latencyspeed');"+
				"    element2.innerHTML = latency; "+

				"    var speed = downloadSpeed();"+
				"    document.formuserdata.score2.value = speed;"+
				"    var element3 = document.getElementById('downloadspeed');"+
				"    element3.innerHTML = speed + ' Kb/s';"+

				"    var param1 = gup('param1');"+
				"    var param2 = gup('param2');"+
				"    var param3 = gup('param3');"+
				"    var lang = gup('lang');"+

				"    document.formuserdata.param1.value = param1;"+
				"    document.formuserdata.param2.value = param2;"+
				"   document.formuserdata.param3.value = param3;"+
				    
				"    document.formuserdata.downloadSpeed.value = speed;"+
				"    document.formuserdata.latencySpeed.value = latency;"+
				"    document.formuserdata.webPerformance.value = score;"+
				"    document.formuserdata.lang.value = lang;"+

				"    var params = $('formuserdata').serialize();"+
				"    var url = '/cron/benchmarkServlet?' + params;"+

				"    http_request = false;"+
				"    if (window.XMLHttpRequest) {"+ 
				"        http_request = new XMLHttpRequest();"+
				"        if (http_request.overrideMimeType) {"+
				"           http_request.overrideMimeType('text/html');"+
				"        }"+
				"    } else if (window.ActiveXObject) {"+ // IE
				"        try {"+
				"            http_request = new ActiveXObject('Msxml2.XMLHTTP');"+
				"        } catch (e) {"+
				"            try {"+
				"                http_request = new ActiveXObject('Microsoft.XMLHTTP');"+
				"            } catch (e) { }"+
				"        }"+
				"    }"+
				"    if (!http_request) {"+
				"        alert('Cannot create XMLHTTP instance');"+
				"        return false;"+
				"    }"+

				"    http_request.open('get', url, true);"+
				"    http_request.send(null);"+
				"    if (lang == 'nl'){"+
				"      document.getElementById('loader').innerHTML = 'Test is uitgevoerd, u kan dit venster sluiten.';"+
				"      document.getElementById('text').innerHTML ='';"+
				"      }"+
				"    else if (lang == 'fr'){"+
				" 		document.getElementById('loader').innerHTML = 'Le test a &eacute;t&eacute; effectu&eacute;, vous pouvez fermer cette fen&ecirc;tre.';"+
				" 		document.getElementById('text').innerHTML = '';"+
				"    }"+
				   
				"}"+

				"function gup(name) {"+
				//"    name = name.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');"+
				"    var regexS = '[\\?&]' + name + '=([^&#]*)';"+
				"    var regex = new RegExp(regexS);"+
				"    var results = regex.exec(window.location.href);"+
				"    if (results == null)"+
				"        return '';"+
				"    else"+
				"        return results[1];"+
				"}"+

				"function fillResolution(json, divname) {"+
				"    var resolution = screen.width + ' X ' + screen.height;"+
				"    document.formuserdata.screenResolution.value = resolution;"+
				"    setTimeout(function() { fillColor(json, 'colorDepth') }, 200);"+
				"}"+

				"function fillJavascript(json, divname) {"+
				"    var enabled ='Yes';"+
				"    document.formuserdata.javascriptEnabled.value = 'Yes';"+
				"    setTimeout(function() { fillResolution(json, 'screenResolution') }, 200);"+
				"}"+

				"function fillFlash(json, divname) {"+
				"    var version = getFlashVersion().split(',').shift();"+
				"    document.formuserdata.flashVersion.value = version;"+
				"    setTimeout(function() { fillbenchmark() }, 2000);"+
				"}"+

				"function fillColor(json, divname) {"+

				"    var colordepth = screen.colorDepth;"+
				"    document.formuserdata.colorDepth.value = colordepth;"+
				"    setTimeout(function() { fillFlash(json, 'flashVersion') }, 200);"+
				"}"+

				"function fillIP(json, divname) {"+
				"    document.formuserdata.ipAddress.value = json.ip;"+
				"    setTimeout(function() { fillJavascript(json, 'javascriptEnabled') }, 200);"+
				"}"+

				"function fillbrowserName(json, divname) {"+
				    
				"    document.formuserdata.browserName.value = json.browserName;"+
				"    document.formuserdata.browserVersion.value = json.browserVersion;"+
				"    setTimeout(function() { fillIP(json, 'ip') }, 200);"+
				"}"+

				"function fillOS(json, divname) {"+
				  
				"    document.formuserdata.platform.value = json.platform;"+
				"    document.formuserdata.platformVersion.value = json.platformVersion;"+
				"    setTimeout(function() { fillbrowserName(json, 'browserName') }, 200);"+
				"}"+
*/
				/*"function build() {"+

				"    var lang = gup('lang');"+
				"    if (lang == 'nl'){"+
				"    document.getElementById('text').innerHTML = 'Performance test wordt doorgevoerd. Gelieve enkele ogenblikken geduld te oefenen. Hartelijk dank voor uw medewerking!'"+
				"    }"+
				"    else if (lang == 'fr'){"+
				"    document.getElementById('text').innerHTML = 'Le test de performance est en cours d&#39; ex&eacute; cution. Veuillez patienter un instant. Merci de votre collaboration!'"+
				"    }"+
				    
				"    var xhr = getJSON();"+
				"    var JSONdata = eval('(' + xhr.responseText + ')');"+
				"    fillOS(JSONdata, 'platform');"+
				"}"+

				"function getFlashVersion() {"+
				"    try {"+
				"        try {"+
				"            var axo = new ActiveXObject('ShockwaveFlash.ShockwaveFlash.6');"+
				"            try { axo.AllowScriptAccess = 'always';}"+
				"            catch (e) { return '6,0,0'; }"+
				"        } catch (e) { }"+
				"       return new ActiveXObject('ShockwaveFlash.ShockwaveFlash').GetVariable('$version')"+
				"    } catch (e) {"+
				"        try {"+
				"            if (navigator.mimeTypes['application/x-shockwave-flash'].enabledPlugin) {"+
				"                return (navigator.plugins['Shockwave Flash 2.0'] || navigator.plugins['Shockwave Flash'])"+
				"            }"+
				"        } catch (e) { }"+
				"    }"+
				"    return '0,0,0';"+
				"}"+
*/


				"function downloadSpeed() {"+

				"    if (window.XMLHttpRequest) {"+
				"        xhttp = new XMLHttpRequest();"+
				"    }"+
				"    else "+
				"    {"+
				"        xhttp = new ActiveXObject('Microsoft.XMLHTTP');"+
				"    }"+

				"    var startTime = new Date().getTime();"+
				"    xhttp.open('GET', '"+systemUrl+"'/data/wallpaper_image.jpg', false);"+
				"    xhttp.send('');"+
				    
				"    xmlDoc = xhttp.responseXML;"+

				"    var endTime =  new Date().getTime();"+

				"    var time = (endTime - startTime) / 1000;"+

				"    var speed = Math.round(1042 / time);"+

				"    return speed;"+
				  
				"}"+
				"function latencySpeed() {"+

				"   if (window.XMLHttpRequest) {"+
				"        xhttp = new XMLHttpRequest();"+
				"    }"+
				"    else"+
				"    {"+
				"        xhttp = new ActiveXObject('Microsoft.XMLHTTP');"+
				"    }"+

				"    var latencySpeed = 0;"+
				"    for(var i=0;i<5;i++){"+

				"            var startTime = new Date().getTime();"+
				"            xhttp.open('GET', '"+systemUrl+"'/data/smaller_image.png', false);"+
				"            xhttp.send('');"+
				"            xmlDoc = xhttp.responseXML;"+
				"            var endTime = new Date().getTime();"+
				"            var time = (endTime - startTime);"+
				"            latencySpeed += time;"+       
				"    }"+
				"    latencySpeed = Math.floor((latencySpeed/5) * 100) / 100;"+
				"    return latencySpeed;"+
				   
				"}"+

				"function javascriptPerformance() {"+

				"    var maxRow = 50;"+
				"    var maxCol = 50;"+

				"    if (window.XMLHttpRequest) {"+
				"        xhttp = new XMLHttpRequest();"+
				"    }"+
				"    else"+
				"    {"+
				"        xhttp = new ActiveXObject('Microsoft.XMLHTTP');"+
				"    }"+
				"    xhttp.open('GET', '"+systemUrl+"'/data/testData.xml', false);"+
				"    xhttp.send('');"+
				"    xmlDoc = xhttp.responseXML;"+

				"    var StartDate = new Date();"+
				"    var StartTime = StartDate.getTime();"+

				"    for (var index = 0; index < 1; index++) {"+

				"        var table = document.createElement('TABLE');"+
				"        for (var iRow = 0; iRow < maxRow; iRow++) {"+
				"            var row = document.createElement('TR');"+
				"            for (var iCol = 0; iCol < maxCol; iCol++) {"+
				"                var cell1 = document.createElement('TD');"+
				"                var cell2 = document.createElement('TD');"+
				"                var checkbox = document.createElement('INPUT');"+
				"                checkbox.type = 'checkbox';"+
				"                cell1.appendChild(checkbox);"+
				"                row.appendChild(cell1);"+
				"                var childNodes = xmlDoc.getElementsByTagName('childLevel1');"+
				"                var iRandom = Math.floor(Math.random() * 100);"+
				"                cell2.innerHTML = childNodes[iRandom].getAttribute('id');"+
				"                row.appendChild(cell2);"+
				"                table.appendChild(row);"+
				"            }"+
				"        }"+
				"    }"+

				"    var EndDate = new Date();"+
				"    var EndTime = EndDate.getTime();"+

				"    var time = (EndTime - StartTime) / 1;"+

				"    var score = time;"+
				"    return score;"+
				"}";

		
		return ret;
		
	}
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}
}
