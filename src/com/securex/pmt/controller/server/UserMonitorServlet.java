package com.securex.pmt.controller.server;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import static com.securex.pmt.fwk.OfyService.ofy;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.securex.pmt.mark.Mark;
import com.securex.pmt.mark.PMF;
import com.securex.pmt.mark.ParseUserAgent;
import com.securex.pmt.mark.UserMonitor;

@SuppressWarnings("serial")
public class UserMonitorServlet extends HttpServlet {
	private EntityManager em = null;
	private static EntityManagerFactory emf = null;
	private static final Logger _logger = Logger
			.getLogger(UserMonitorServlet.class.getName());
	public class ResponseObject {
		public Object data = null;
	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		HttpSession session = req.getSession(true);
		ResponseObject respObj = new ResponseObject();
		if(req.getParameter("getUserLists") != null){  
			HashMap<String, Object> returnJson = new HashMap<String, Object>();
			String condStr = "";
			//returnJson.put("userLists",  ofy().load().type(UserMonitor.class).order("email").list());
			PersistenceManager pm;
			pm = PMF.get().getPersistenceManager();
			//Query query = pm.newQuery(com.fourture.mark.Mark.class);
			Query query = pm.newQuery(com.securex.pmt.mark.UserMonitor.class, condStr);
			query.setOrdering( "username ascending");

			//List<Mark> results = (List<Mark>) query.execute();
			List<UserMonitor> results = (List<UserMonitor>) query.execute();
			if(results.isEmpty()){
				respObj.data = "";
			}else{
				respObj.data = results;
			}
			 
		}else if(req.getParameter("deleteUser") != null){ 
			String username = req.getParameter("username");
			String condStr = "";
			Map params = new HashMap();
			PersistenceManager pm;
			pm = PMF.get().getPersistenceManager();
			Query q = pm.newQuery(com.securex.pmt.mark.UserMonitor.class);
			q.setFilter("username == emailParam");
			q.declareParameters("String emailParam");

			try {
			  List<UserMonitor> results = (List<UserMonitor>) q.execute(username);
			  if (!results.isEmpty()) {
			    for (UserMonitor p : results) {
			    	pm.deletePersistent(p);
			      // Process result p
			    }
			  } else {
			    // Handle "no results" case
			  }
			} finally {
			  q.closeAll();
			}
			
		}else if(req.getParameter("getUserResults") != null){ 
			String username = req.getParameter("username");
			String condStr = "";
			Map params = new HashMap();
			PersistenceManager pm;
			pm = PMF.get().getPersistenceManager();
			Query q = pm.newQuery(com.securex.pmt.mark.Mark.class);
			q.setFilter("param3 == emailParam");
			q.declareParameters("String emailParam");

			try {
			  List<Mark> resultsMark = (List<Mark>) q.execute(username);
			  if (!resultsMark.isEmpty()) {
				  respObj.data = resultsMark;
			  
			  } else {
			    // Handle "no results" case
			  }
			} finally {
			  q.closeAll();
			}
			
		}else{
			try {
				
				// save results
				UserMonitor user = new UserMonitor();

				String useragent = req.getHeader("User-Agent");
				ParseUserAgent p = new ParseUserAgent( useragent);
				
				user.setUsername(req.getParameter("username"));
				user.setMeasureModus(req.getParameter("optmeasure"));
				user.setTimestamp(Calendar.getInstance().getTime());
			
				// register result
				PersistenceManager pm;
				pm = PMF.get().getPersistenceManager();

				pm.makePersistent(user);
				pm.close();
				

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				// GreetingServiceImpl.log = GreetingServiceImpl.log + " " +
				// e.toString();
			}
		}
		  resp.getWriter().println(new Gson().toJson(respObj)); 
		
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}
}
