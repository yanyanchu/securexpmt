
package com.securex.pmt.fwk;

import com.securex.pmt.mark.*;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;


public final class OfyService {
	
	static {
		
		factory().register(UserMonitor.class);
	}

	private OfyService() {}

    public static Objectify ofy() {
        return ObjectifyService.ofy();
    }

    public static ObjectifyFactory factory() {
        return ObjectifyService.factory();
    }
}